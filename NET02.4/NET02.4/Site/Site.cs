﻿using System;
using System.Net;
using System.Threading.Tasks;
using NLog;

namespace NET02._4.Site
{
    public class Site
    {
        private readonly Mail _mail;
        private readonly HttpWebRequest _request;
        public int Interval { get; }
        public int Timeout { get; }
        public string Url { get; }

        public Site(string url, int interval, int timeOut, Mail mail)
        {
            Url = url;
            _mail = mail;
            Timeout = timeOut;
            Interval = interval;
            try
            {
                _request = (HttpWebRequest) WebRequest.Create(Url);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e);
            }

            _request.Timeout = Timeout * 1000;
        }

        public HttpWebResponse SiteRequest()
        {
            return (HttpWebResponse) _request.GetResponse();
        }

        public void SendMailAsync(string message)
        {
            Task.Run(() => _mail.SendMailAsync(message));
        }
    }
}