﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using NLog;

namespace NET02._4.Site
{
    public class Mail
    {
        private readonly MailMessage _mailMessage;
        private readonly SmtpClient _smtpClient;

        public Mail(string from, string to, string theme, string password, string host)
        {
            _mailMessage = new MailMessage(from, to)
            {
                Subject = theme
            };
            _smtpClient = new SmtpClient(host)
            {
                Credentials = new NetworkCredential(from, password),
                EnableSsl = true
            };
        }

        public async Task SendMailAsync(string message)
        {
            try
            {
                _mailMessage.Body = message;
                await _smtpClient.SendMailAsync(_mailMessage);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e);
            }
        }
    }
}