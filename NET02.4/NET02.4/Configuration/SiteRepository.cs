﻿using System;
using System.Collections.Generic;
using System.Configuration;
using NET02._4.Site;

namespace NET02._4.Configuration
{
    public static class SiteRepository
    {
        public static IEnumerable<Site.Site> GetSities()
        {
            ConfigurationManager.RefreshSection("AppSection");
            var _configuration = ConfigurationManager.GetSection("AppSection") as AppSectionConfig;
            if (_configuration == null)
            {
                throw new ArgumentNullException($"AppSection", "is not found in config file");
            }

            foreach (var item in _configuration.Sites)
            {
                var obj = item as SiteConfiguration;
                var mail = obj.Email;
                yield return new Site.Site(obj.Url, obj.Interval, obj.TimeOut, new Mail(
                    mail.From, mail.Mail, mail.Theme, mail.Password, mail.Host));
            }
        }
    }
}