﻿using System.Configuration;

namespace NET02._4.Configuration
{
    [ConfigurationCollection(typeof(SiteConfiguration), AddItemName = "Site",
        CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class SitesCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteConfiguration) element).Url;
        }
    }
}