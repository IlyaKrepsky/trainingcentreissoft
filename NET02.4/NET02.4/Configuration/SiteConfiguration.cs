﻿using System.Configuration;

namespace NET02._4.Configuration
{
    public class SiteConfiguration : ConfigurationElement
    {
        [ConfigurationProperty("url", IsRequired = true, DefaultValue = "www.example.com")]
        public string Url
        {
            get => (string) base["url"];
            set => base["url"] = value;
        }

        [ConfigurationProperty("interval", IsRequired = true, DefaultValue = 11)]
        [IntegerValidator(MinValue = 10, MaxValue = 360)]
        public int Interval
        {
            get => (int) base["interval"];
            set => base["interval"] = value;
        }

        [ConfigurationProperty("timeout", IsRequired = true, DefaultValue = 3)]
        [IntegerValidator(MinValue = 3, MaxValue = 9)]
        public int TimeOut
        {
            get => (int) base["timeout"];
            set => base["timeout"] = value;
        }

        [ConfigurationProperty("EmailProp", IsRequired = true)]
        public EmailConfiguration Email => (EmailConfiguration) base["EmailProp"];
    }
}