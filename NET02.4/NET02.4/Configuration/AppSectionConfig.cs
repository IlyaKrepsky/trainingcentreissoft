﻿using System.Configuration;

namespace NET02._4.Configuration
{
    public class AppSectionConfig : ConfigurationSection
    {
        [ConfigurationProperty("Sites", IsRequired = true)]
        public SitesCollection Sites => (SitesCollection) base["Sites"];
    }
}