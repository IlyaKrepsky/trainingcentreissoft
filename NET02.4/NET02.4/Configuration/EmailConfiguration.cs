﻿using System.Configuration;

namespace NET02._4.Configuration
{
    public class EmailConfiguration : ConfigurationElement
    {
        [ConfigurationProperty("mail", IsRequired = true)]
        public string Mail
        {
            get => (string) base["mail"];
            set => base["mail"] = value;
        }

        [ConfigurationProperty("from", IsRequired = true)]
        public string From
        {
            get => (string) base["from"];
            set => base["from"] = value;
        }

        [ConfigurationProperty("theme", IsRequired = true)]
        public string Theme
        {
            get => (string) base["theme"];
            set => base["theme"] = value;
        }

        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get => (string) base["password"];
            set => base["password"] = value;
        }

        [ConfigurationProperty("host", IsRequired = true)]
        public string Host
        {
            get => (string) base["host"];
            set => base["host"] = value;
        }
    }
}