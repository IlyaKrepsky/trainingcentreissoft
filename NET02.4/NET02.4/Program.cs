﻿using System;
using System.Reflection;
using System.Threading;
using Monitor = NET02._4.SiteMonitor.Monitor;

namespace NET02._4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (var mutex = new Mutex(false, Assembly.GetExecutingAssembly().GetName().Name))
            {
                if (!mutex.WaitOne(TimeSpan.FromSeconds(3), false))
                {
                    Console.WriteLine("Another instance is running");
                    return;
                }

                RunProgram();
            }
        }

        private static void RunProgram()
        {
            var monitor = new Monitor();
            Console.ReadKey();
        }
    }
}