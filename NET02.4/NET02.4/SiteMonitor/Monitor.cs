﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using NET02._4.Configuration;
using NLog;

namespace NET02._4.SiteMonitor
{
    public class Monitor
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private CountdownEvent _counter;
        private bool _flag;

        public Monitor()
        {
            var configFile = string.Concat(Assembly.GetEntryAssembly().Location, ".config");
            var watcher = new FileSystemWatcher(Path.GetDirectoryName(configFile), Path.GetFileName(configFile))
            {
                EnableRaisingEvents = true
            };
            watcher.Changed += ReInitThreads;
            InitThreads();
        }

        private void ReInitThreads(object o, FileSystemEventArgs e)
        {
            _flag = false;
            _counter.Wait();
            InitThreads();
        }

        private void InitThreads()
        {
            try
            {
                _flag = true;
                _counter = new CountdownEvent(SiteRepository.GetSities().Count());
                foreach (var item in SiteRepository.GetSities())
                {
                    var thread = new Thread(SiteRequest);
                    thread.Start(item);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        private void SiteRequest(object obj)
        {
            var site = obj as Site.Site;
            var count = 0;
            try
            {
                while (_flag)
                {
                    Thread.Sleep(site.Interval * 1000);
                    var response = site.SiteRequest();
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                        {
                            count = count != 0 ? 0 : count;
                            _logger.Info(
                                $"website {site.Url} {response.StatusCode}");
                        }
                            break;
                        case HttpStatusCode.NotFound:
                        {
                            count++;
                            if (count >= 3)
                            {
                                site.SendMailAsync($"website {site.Url} unavailable ");
                            }
                        }
                            break;
                        default: break;
                    }

                    response.Close();
                }
            }
            catch (Exception e)
            {
                site.SendMailAsync($"{site.Url}  {e}");
            }

            _counter.Signal();
        }
    }
}