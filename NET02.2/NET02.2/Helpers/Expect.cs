﻿using System;

namespace NET02._2.Helpers
{
    internal static class Expect
    {
        public static void ArgumentIsNotNull<T>(T argument, string argumentName) where T: class
        {
            if (argument == null)
            {
                throw new ArgumentNullException(argumentName, "is null");
            }
        }

        public static void ArgumentNonNegative(int argument, string argumentName)
        {
            if (argument <= 0)
            {
                throw new ArgumentOutOfRangeException(argumentName, "must be positive");
            }
        }
    }
}