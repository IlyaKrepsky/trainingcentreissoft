﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Newtonsoft.Json;
using NET02._2.Helpers;

namespace NET02._2.Entities
{
    public class User
    {
        private readonly List<Window> _windows;

        public string Name { get; private set; }

        public bool IsCorrect => _windows.FirstOrDefault(t =>
                                     t.Title == "main" && t.IsCorrect) != null;

        public User()
        {
            _windows = new List<Window>();
        }

        public void ReadFromXml(XElement element)
        {
            try
            {
                Name = element.Attribute("name")?.Value;
                Expect.ArgumentIsNotNull(Name, nameof(Name));
                foreach (var wnd in element.Elements("window"))
                {
                    var window = new Window();
                    window.ReadFromXml(wnd);
                    _windows.Add(window);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void SerializeToJsonFile(DirectoryInfo dir)
        {
            SetDefaultValues();
            try
            {
                var subdir = dir.CreateSubdirectory(Name);
                using (var file = new StreamWriter(subdir.FullName + @"/" + Name + @".json"))
                {
                    var json = new StringBuilder();
                    foreach (var win in _windows)
                    {
                        json.AppendLine(JsonConvert.SerializeObject(win, Formatting.None));
                    }

                    file.Write(json.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        public void SetDefaultValues()
        {
            if (_windows.FirstOrDefault(w => w.Title == "main") != null)
            {
                _windows.ForEach(w => w.SetDefaultValues());
            }
            else
            {
                var window = new Window();
                _windows.Insert(0, window);
                _windows.ForEach(w => w.SetDefaultValues());
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendLine("Login:" + Name);
            foreach (var window in _windows)
            {
                result.AppendLine(" " + window);
            }

            return result.ToString();
        }
    }
}