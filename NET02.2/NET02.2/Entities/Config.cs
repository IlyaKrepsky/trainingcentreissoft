﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using NET02._2.Helpers;

namespace NET02._2.Entities
{
    internal class Config
    {
        private readonly string _dirPath = Directory.GetCurrentDirectory() + @"\config";
        private readonly string _xmlPath = Directory.GetCurrentDirectory() + @"\config\config.xml";
        private readonly List<User> _users;

        public Config()
        {
            _users = new List<User>();
            ReadFromXml(_xmlPath);
        }

        public IEnumerable<User> GetInCorrectUser()
        {
            return _users.Where(u => !u.IsCorrect);
        }

        private void ReadFromXml(string path)
        {
            try
            {
                var doc = XDocument.Load(path);
                var query = doc.Root?.Elements("login");
                Expect.ArgumentIsNotNull(query, "config.xml");
                foreach (var element in query)
                {
                    var user = new User();
                    user.ReadFromXml(element);
                    _users.Add(user);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void SerializeToJsonFiles()
        {
            var dir = new DirectoryInfo(_dirPath);
            foreach (var user in _users)
            {
                user.SerializeToJsonFile(dir);
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            foreach (var user in _users)
            {
                result.AppendLine(user.ToString());
            }

            return result.ToString();
        }
    }
}