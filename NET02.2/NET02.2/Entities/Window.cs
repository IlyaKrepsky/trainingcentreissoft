﻿using System;
using System.Xml.Linq;
using NET02._2.Helpers;

namespace NET02._2.Entities
{
    public class Window
    {
        public string Title { get; private set; }
        public int? Top { get; private set; }
        public int? Left { get; private set; }
        public int? Width { get; private set; }
        public int? Heigth { get; private set; }

        public bool IsCorrect => Top.HasValue && Left.HasValue && Width.HasValue && Heigth.HasValue;

        public void ReadFromXml(XElement xml)
        {
            try
            {
                Title = xml.Attribute("title")?.Value;
                Expect.ArgumentIsNotNull(Title, nameof(Title));
                foreach (var element in xml.Elements())
                {
                    ParseXElement(element);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public override string ToString()
        {
            return $"{Title} ({Top}, {Left}, {Width}, {Heigth})";
        }

        public void SetDefaultValues()
        {
            Title = Title ?? "main";
            Top = Top ?? 0;
            Left = Left ?? 0;
            Width = Width ?? 400;
            Heigth = Heigth ?? 150;
        }

        private void ParseXElement(XElement element)
        {
            switch (element.Name.ToString())
            {
                case "top":
                {
                    Top = Convert.ToInt32(element.Value);
                }
                    break;
                case "left":
                {
                    Left = Convert.ToInt32(element.Value);
                }
                    break;
                case "width":
                {
                    var value = Convert.ToInt32(element.Value);
                    Expect.ArgumentNonNegative(value, nameof(Width));
                    Width = Convert.ToInt32(element.Value);
                }
                    break;
                case "height":
                {
                    var value = Convert.ToInt32(element.Value);
                    Expect.ArgumentNonNegative(value, nameof(Heigth));
                    Heigth = value;
                }
                    break;
                default:
                    break;
            }
        }
    }
}