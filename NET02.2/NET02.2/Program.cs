﻿using System;
using System.Linq;
using NET02._2.Entities;

namespace NET02._2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var conf = new Config();

            Console.WriteLine(conf.ToString());

            Console.ReadKey();

            var user = conf.GetInCorrectUser().ToList();

            foreach (var us in user)
            {
                Console.WriteLine(us.ToString());
            }

            conf.SerializeToJsonFiles();
            Console.WriteLine(conf.ToString());

            Console.ReadKey();
        }
    }
}