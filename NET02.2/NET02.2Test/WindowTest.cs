﻿using System.Xml.Linq;
using NET02._2.Entities;
using NUnit.Framework;

namespace NET02._2Test
{
    [TestFixture]
    public class WindowTest
    {
        [Test]
        public void IsCorrect_SetIncorrectValues_ReturnFalse()
        {
            //Arrange
            var window = new Window();
            var xml = new XElement("window",
                new XAttribute("title", "main"),
                new XElement("top", 20), new XElement("width", 30));
            //Act
            window.ReadFromXml(xml);
            var flag = window.IsCorrect;
            //Assert
            Assert.That(flag, Is.False);
        }

        [Test]
        public void IsCorrect_SetCorrectValues_ReturnTrue()
        {
            //Arrange
            var window = new Window();
            var xml = new XElement("window",
                new XAttribute("title", "main"),
                new XElement("top", 20), new XElement("width", 30),
                new XElement("left", 20), new XElement("heigth", 30)
            );
            //Act
            window.ReadFromXml(xml);
            var flag = window.IsCorrect;
            //Assert
            Assert.That(flag, Is.False);
        }

        [Test]
        public void SetDefaultValues_EmptyValuesWindow_ReturnDefaultValues()
        {
            //Arrange
            var window = new Window();
            //Act
            window.SetDefaultValues();
            //Assert
            Assert.That(window.Title, Is.EqualTo("main"));
            Assert.That(window.Top, Is.EqualTo(0));
            Assert.That(window.Left, Is.EqualTo(0));
            Assert.That(window.Width, Is.EqualTo(400));
            Assert.That(window.Heigth, Is.EqualTo(150));
        }

        [Test]
        public void SetDefaultValues_NotEmptyValuesWindow_ReturnValidValues()
        {
            //Arrange
            var window = new Window();
            var xml = new XElement("window",
                new XAttribute("title", "help"),
                new XElement("top", 20), new XElement("width", 30)
            );
            //Act
            window.ReadFromXml(xml);
            window.SetDefaultValues();
            //Assert
            Assert.That(window.Title, Is.EqualTo("help"));
            Assert.That(window.Top, Is.EqualTo(20));
            Assert.That(window.Left, Is.EqualTo(0));
            Assert.That(window.Width, Is.EqualTo(30));
            Assert.That(window.Heigth, Is.EqualTo(150));
        }
    }
}