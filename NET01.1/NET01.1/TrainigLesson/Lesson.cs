﻿using System;
using NET01._1.Base;
using NET01._1.Enums;
using NET01._1.Interfaces;
using NET01._1.LessonsMaterial;

namespace NET01._1.TrainigLesson
{
    public class Lesson : Base.Base, IVersionable, ICloneable
    {
        private byte[] _version;

        public BaseMaterial[] Materials { get; set; } 

        public TypeOfLesson LessonType()
        {
            foreach (var material in Materials)
            {
                if (material is VideoMaterial)
                {
                    return TypeOfLesson.VideoLesson;
                }
            }
            return TypeOfLesson.TextLesson;
        }

        public byte[] GetVersion()
        {
            return _version;
        }

        public void SetVersion(byte [] version)
        {
            if (version.Length != 8)
            {
                throw new ArgumentException();
            }
            _version = version;
        }

        public object Clone()
        {
            var lesson = new Lesson()
            {
                Id = Id,
                Description = Description
            };
            var vers = GetVersion().CopyVersion();          
            lesson.SetVersion(vers);
            if (Materials == null)
            {
                return lesson;
            }
            lesson.Materials = new BaseMaterial[Materials.Length];
            for (var i = 0; i < Materials.Length; i++)
            {
                lesson.Materials[i] = Materials[i].CloneMaterial();
            }
            return lesson;
        }
        
    }
}