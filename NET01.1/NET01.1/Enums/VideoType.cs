﻿namespace NET01._1.Enums
{
    internal enum VideoType
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}
