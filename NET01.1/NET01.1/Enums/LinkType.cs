﻿namespace NET01._1.Enums
{
    public enum LinkType
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}