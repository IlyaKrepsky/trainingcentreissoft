﻿using System;
using NET01._1.Base;
using NET01._1.Enums;
using NET01._1.Interfaces;

namespace NET01._1.LessonsMaterial
{
    internal class VideoMaterial : BaseMaterial, IVersionable
    {
        private string _uriImage;
        private string _uriVideo;
        private byte[] _version;
     
        public VideoType VideoType { get; set; }

        public string UriVideo
        {
            get => _uriVideo;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                _uriVideo = value;
            }
        }

        public string UriImage
        {
            get => _uriImage;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                _uriImage = value;
            }
        }

        public byte[] GetVersion()
        {
            return _version;
        }

        public void SetVersion(byte[] version)
        {
            if (version.Length != 8)
            {
                throw new ArgumentException();
            }
            _version = version;
        }

        public override BaseMaterial CloneMaterial()
        {
           var videoMaterial = new VideoMaterial()
           {
               Id=Id,
               UriVideo = UriVideo,
               UriImage = UriImage,
               VideoType = VideoType,
           };
            var vers = GetVersion().CopyVersion();
            videoMaterial.SetVersion(vers);
            return videoMaterial;
        }

    }
}