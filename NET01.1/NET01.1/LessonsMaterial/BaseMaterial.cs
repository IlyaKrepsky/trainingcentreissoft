﻿using System;

namespace NET01._1.LessonsMaterial
{
    public abstract class BaseMaterial : Base.Base
    {
        public abstract BaseMaterial CloneMaterial();
    }
}
