﻿using System;

namespace NET01._1.LessonsMaterial
{
    public class TextMaterial : BaseMaterial
    {
        private const int MaxTextLength = 10000;
        private string _text;

        public string Text
        {
            get => _text;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                _text = value.Length > MaxTextLength ? throw  new ArgumentException(): value;
            }
        }
        
        public override BaseMaterial CloneMaterial()
        {
            var textMaterial = new TextMaterial()
            {
                Description = Description,
                Text = Text,
                Id = this.Id
            };
            return textMaterial;
        }
    }
}