﻿using System;
using NET01._1.Enums;

namespace NET01._1.LessonsMaterial
{
    public class LinkMaterial : BaseMaterial
    {
        private string _uri;

        public LinkType LinkType { get; set; }

        public string Uri
        {
            get => _uri;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                _uri = value;
            }
        }

        public override BaseMaterial CloneMaterial()
        {
            var linkMaterial = new LinkMaterial()
            {
                Description = Description,
                Uri = Uri,
                LinkType = LinkType,
                Id = Id
            };
            return linkMaterial;
        }
    }
}