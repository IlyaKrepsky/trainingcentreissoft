﻿using System;

namespace NET01._1.Base
{
    public static class Helper
    {
        public static void SetGuid(this Base obj)
        {
            obj.Id = Guid.NewGuid();
        }

        internal static byte[] CopyVersion(this byte[] obj)
        {
            var bytes = new byte[obj.Length];
            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] = obj[i];
            }
            return bytes;
        }
    }
}
