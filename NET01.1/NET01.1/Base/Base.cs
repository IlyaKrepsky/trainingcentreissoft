﻿using System;

namespace NET01._1.Base
{
    public abstract class Base
    {
        private const int MaxLenght = 256;
        private string _description;

        public string Description
        {
            get => _description;
            set
            {
                if (value.Length >= MaxLenght)
                {
                   throw new ArgumentException();
                }

                _description = value;
            }
        }

        public Guid Id { get; set; }

        public override string ToString()
        {
            return _description;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Base b))
            {
                return false;
            }

            return Id.Equals(b.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}