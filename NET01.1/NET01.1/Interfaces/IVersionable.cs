﻿namespace NET01._1.Interfaces
{
    public interface IVersionable
    {
      
        byte[] GetVersion();
        void SetVersion(byte [] version);
    }
}