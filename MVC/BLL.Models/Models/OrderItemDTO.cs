﻿using System;

namespace BLL.Models.Models
{
    public class OrderItemDTO
    {
        public Guid OrderNumber { get; set; }
        public ProductDTO Product { get; set; }
        public int Count { get; set; }
    }
}