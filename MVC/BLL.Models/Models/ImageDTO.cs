﻿using System;

namespace BLL.Models.Models
{
    public class ImageDTO
    {
        public Guid Name { get; set; }
        public byte[] Data { get; set; }
        public string MimeType { get; set; }
        public Guid ProductNumber { get; set; }
    }
}