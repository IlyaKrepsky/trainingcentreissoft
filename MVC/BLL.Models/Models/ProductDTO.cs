﻿using System;

namespace BLL.Models.Models
{
    public class ProductDTO
    {
        public string Name { get; set; }
        public Guid Number { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public int Quantity { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
    }
}