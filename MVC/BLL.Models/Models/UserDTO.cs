﻿using System.Collections.Generic;

namespace BLL.Models.Models
{
    public class UserDTO
    {
        public string UserName { get; set; }
        public string RealName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}