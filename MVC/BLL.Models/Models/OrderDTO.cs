﻿using System;
using System.Collections.Generic;

namespace BLL.Models.Models
{
    public class OrderDTO
    {
        public Guid Number { get; set; }
        public DateTime Date { get; set; }
        public string UserEmail { get; set; }
        public IEnumerable<OrderItemDTO> OrderItems { get; set; }
    }
}