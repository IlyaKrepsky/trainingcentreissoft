﻿using System;
using DAL.Context.Context;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;

namespace DAL.Context.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WebShopContext _dbContext;

        private IRepository<Order> _orderRepository;
        private IRepository<User> _userRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<Product> _productRepository;
        private IRepository<Category> _categoryRepository;
        private IRepository<Brand> _brandRepository;
        private IRepository<OrderItem> _orderItemRepository;
        private IRepository<Image> _pictureRepository;

        private bool _disposed;

        public IRepository<Order> OrderRepository =>
            _orderRepository ?? (_orderRepository = new Repository<Order>(_dbContext));

        public IRepository<User> UserRepository =>
            _userRepository ?? (_userRepository = new Repository<User>(_dbContext));

        public IRepository<Role> RoleRepository =>
            _roleRepository ?? (_roleRepository = new Repository<Role>(_dbContext));

        public IRepository<Product> ProductRepository =>
            _productRepository ?? (_productRepository = new Repository<Product>(_dbContext));

        public IRepository<Category> CategoryRepository =>
            _categoryRepository ?? (_categoryRepository = new Repository<Category>(_dbContext));

        public IRepository<Brand> BrandRepository =>
            _brandRepository ?? (_brandRepository = new Repository<Brand>(_dbContext));

        public IRepository<OrderItem> OrderItemRepository =>
            _orderItemRepository ?? (_orderItemRepository = new Repository<OrderItem>(_dbContext));

        public IRepository<Image> PictureRepository =>
            _pictureRepository ?? (_pictureRepository = new Repository<Image>(_dbContext));

        public UnitOfWork(string connectionString)
        {
            _dbContext = new WebShopContext(connectionString);
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _dbContext.Dispose();
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}