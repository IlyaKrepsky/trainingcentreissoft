﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    internal class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            ToTable("Categories").HasKey(b => b.Id);
            Property(c => c.Name).IsRequired().HasMaxLength(200);
            Property(c => c.Description).IsOptional().HasMaxLength(1000);
        }
    }
}