﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    internal class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            ToTable("Products").HasKey(p => p.Id);

            Property(p => p.Name).IsRequired().HasMaxLength(200);
            Property(p => p.Description).IsOptional().HasMaxLength(1000);
            Property(p => p.Cost).IsRequired().HasPrecision(8, 2);

            HasRequired(p => p.Category)
                .WithMany(c => c.Products)
                .Map(m => m.MapKey("CategoryId"))
                .WillCascadeOnDelete(true);

            HasRequired(p => p.Brand)
                .WithMany(b => b.Products)
                .Map(m => m.MapKey("ManufacturerId"))
                .WillCascadeOnDelete(true);
        }
    }
}