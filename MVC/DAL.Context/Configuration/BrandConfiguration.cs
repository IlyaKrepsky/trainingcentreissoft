﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    internal class BrandConfiguration : EntityTypeConfiguration<Brand>
    {
        public BrandConfiguration()
        {
            ToTable("Manufacturers").HasKey(b => b.Id);
            Property(b => b.Name).IsRequired().HasMaxLength(200);
            Property(c => c.Description).IsOptional().HasMaxLength(1000);
        }
    }
}