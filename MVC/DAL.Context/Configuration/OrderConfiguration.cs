﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    internal class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            ToTable("Orders").HasKey(o => o.Id);
            Property(p => p.Number).IsRequired();
            Property(p => p.Date).IsRequired();

            HasRequired(o => o.User)
                .WithMany(u => u.Orders)
                .Map(m => m.MapKey("UserId"))
                .WillCascadeOnDelete(true);
        }
    }
}