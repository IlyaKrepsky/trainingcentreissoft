﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    internal class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("Users").HasKey(u => u.Id);

            Property(u => u.UserName).IsRequired().HasMaxLength(100);
            Property(u => u.RealName).IsRequired().HasMaxLength(200);
            Property(u => u.Email).IsRequired().HasMaxLength(200);
            Property(u => u.Password).IsRequired().HasMaxLength(256);
            Property(u => u.Phone).IsRequired().HasMaxLength(20);

            HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("UsersRoles");
                    m.MapRightKey("UserId");
                    m.MapLeftKey("RoleId");
                });
        }
    }
}