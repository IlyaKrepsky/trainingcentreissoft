﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    public class ImageConfiguration : EntityTypeConfiguration<Image>
    {
        public ImageConfiguration()
        {
            ToTable("Images").HasKey(p => p.Id);
            Property(p => p.Name).IsRequired();
            Property(p => p.Data).IsRequired();
            Property(p => p.MimeType).IsRequired().HasMaxLength(10);

            HasRequired(p => p.Product)
                .WithMany(c => c.Images)
                .Map(m => m.MapKey("ProductId"))
                .WillCascadeOnDelete(true);
        }
    }
}