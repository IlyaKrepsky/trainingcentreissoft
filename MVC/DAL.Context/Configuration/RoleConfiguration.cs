﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    internal class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleConfiguration()
        {
            ToTable("Roles").HasKey(r => r.Id);

            Property(p => p.Name).IsRequired().HasMaxLength(50);

            HasMany(c => c.Users)
                .WithMany(u => u.Roles)
                .Map(m =>
                {
                    m.ToTable("UsersRoles");
                    m.MapRightKey("UserId");
                    m.MapLeftKey("RoleId");
                });
        }
    }
}