﻿using System.Data.Entity.ModelConfiguration;
using DAL.Entities.Entities;

namespace DAL.Context.Configuration
{
    internal class OrderItemConfiguration : EntityTypeConfiguration<OrderItem>
    {
        public OrderItemConfiguration()
        {
            ToTable("OrderDetails").HasKey(o => o.Id);

            HasRequired(o => o.Product)
                .WithMany(p => p.OrderItems)
                .Map(m => m.MapKey("ProductId"))
                .WillCascadeOnDelete();

            HasRequired(o => o.Order)
                .WithMany(r => r.OrderItems)
                .Map(m => m.MapKey("OrderId"))
                .WillCascadeOnDelete();
        }
    }
}