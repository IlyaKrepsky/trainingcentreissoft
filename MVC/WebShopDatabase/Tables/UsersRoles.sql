﻿CREATE TABLE [dbo].[UsersRoles]
(
	[UserId] Int NOT NULL,
	[RoleId] Int NOT NULL, 
    CONSTRAINT [PK_Users_Roles] PRIMARY KEY ([UserId], [RoleId]), 
    CONSTRAINT [FK_Users_Roles_Users] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_Users_Roles_Roles] FOREIGN KEY ([RoleId]) REFERENCES [Roles]([Id]) ON DELETE CASCADE
)
