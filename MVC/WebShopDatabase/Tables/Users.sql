﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[UserName] NVARCHAR(100) NOT NULL,
	[RealName] NVARCHAR(200) NOT NULL, 
	[Email] NVARCHAR(200) NOT NULL,
	[Password] VARBINARY(256) NOT NULL,
	[Phone] NVARCHAR(20) NOT NULL,

	CONSTRAINT [PK_Users] PRIMARY KEY ([Id]),
)
GO

CREATE UNIQUE INDEX AK_Users_Email ON [dbo].[Users] (Email)
GO

CREATE UNIQUE INDEX AK_Users_Login ON [dbo].[Users] ([UserName]);
GO
