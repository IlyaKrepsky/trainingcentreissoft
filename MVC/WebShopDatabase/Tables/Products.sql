﻿CREATE TABLE [dbo].[Products]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Number] UNIQUEIDENTIFIER DEFAULT NEWID(),
	[Name] NVARCHAR(200) NOT NULL,
	[Description] NVARCHAR(1000),
	[Cost] DECIMAL(8, 2) NOT NULL,
	[Quantity] INT DEFAULT 0,
	[CategoryId] INT NOT NULL,
	[ManufacturerId] INT NOT NULL

	CONSTRAINT [PK_Products] PRIMARY KEY ([Id]), 
	CONSTRAINT [FK_Products_Categories] FOREIGN KEY ([CategoryId]) REFERENCES [Categories]([Id]) ON DELETE CASCADE, 
	CONSTRAINT [FK_Products_Manufacturers] FOREIGN KEY ([ManufacturerId]) REFERENCES [Manufacturers]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [CK_Products_Quantity] CHECK (Quantity >= 0), 
    CONSTRAINT [CK_Products_Cost] CHECK (Cost > 0),

)
GO

CREATE INDEX [IX_Products_Name] ON [dbo].[Products] ([Name])
GO

CREATE INDEX [IX_Products_CategoryId] ON [dbo].[Products] ([CategoryId])
GO

CREATE INDEX [IX_Products_ManufacturerId] ON [dbo].[Products] ([ManufacturerId])

GO

CREATE UNIQUE INDEX [AK_Products_Number] ON [dbo].[Products] ([Number])
