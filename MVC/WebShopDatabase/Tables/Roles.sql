﻿CREATE TABLE [dbo].[Roles]
(
	[Id] INT NOT NULL IDENTITY(1, 1), 
	[Name] NVARCHAR(50) NOT NULL UNIQUE, 
	CONSTRAINT [PK_Roles] PRIMARY KEY ([Id]), 
)

GO
CREATE UNIQUE INDEX [AK_Roles_Name] ON [dbo].[Roles] ([Name])
