﻿CREATE TABLE [dbo].[Manufacturers]
(
	[Id] INT IDENTITY(1, 1) NOT NULL,
	[Name] NVARCHAR(200) NOT NULL,
	[Description] NVARCHAR(1000), 
	CONSTRAINT [PK_Manufacturers] PRIMARY KEY ([Id])
)
GO

CREATE UNIQUE INDEX [AK_Manufacturers_Name] ON [dbo].[Manufacturers] ([Name])
