﻿CREATE TABLE [dbo].[Images]
(
	[Id] INT IDENTITY(1, 1) NOT NULL,
	[Name] UNIQUEIDENTIFIER DEFAULT NEWID(),
	[Data] VARBINARY (MAX) NOT NULL,
	[MimeType] NVARCHAR (10) NOT NULL,
	[ProductId] INT NOT NULL, 

	CONSTRAINT [PK_Images] PRIMARY KEY ([Id]), 
	CONSTRAINT [FK_Images_Products] FOREIGN KEY ([ProductId]) REFERENCES [Products]([Id]) ON DELETE CASCADE,
)
GO

CREATE INDEX [IX_Images_ProductId] ON [dbo].[Images] ([ProductId])

GO

CREATE UNIQUE INDEX [AK_Images_Name] ON [dbo].[Images] ([Name])
