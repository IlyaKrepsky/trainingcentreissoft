﻿CREATE TABLE [dbo].[Orders]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Number] UNIQUEIDENTIFIER DEFAULT NEWID(),
	[Date] DATETIME DEFAULT GETDate(),
	[UserId] INT NOT NULL,

	CONSTRAINT [PK_Orders] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_Orders_USerr] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]) ON DELETE CASCADE, 
)
GO

CREATE UNIQUE INDEX [AK_Orders_Number] ON [dbo].[Orders]([Number])
GO

CREATE INDEX [IX_Orders_UserId] ON [dbo].[Orders] ([UserId])
