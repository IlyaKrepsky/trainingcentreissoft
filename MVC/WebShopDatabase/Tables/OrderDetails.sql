﻿CREATE TABLE [dbo].[OrderDetails]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[OrderId] INT NOT NULL,
	[ProductId] INT NOT NULL,
	[ProductCount] INT NOT NULL, 
    CONSTRAINT [PK_OrderDetails] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_OrderDetails_Orders] FOREIGN KEY ([OrderId]) REFERENCES [Orders]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_OrderDetails_Products] FOREIGN KEY ([ProductId]) REFERENCES [Products]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [CK_OrderDetails_ProductCount] CHECK (ProductCount > 0)
)

GO

CREATE UNIQUE INDEX [AK_OrderDetails_Order_Product] ON [dbo].[OrderDetails] ([OrderId], [ProductId])
