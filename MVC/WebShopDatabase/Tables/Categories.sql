﻿CREATE TABLE [dbo].[Categories]
(
	[Id] INT NOT NULL IDENTITY(1, 1),
	[Name] NVARCHAR(200) NOT NULL,
	[Description] NVARCHAR(1000), 
	CONSTRAINT [PK_Categories] PRIMARY KEY ([Id])
)
GO

CREATE UNIQUE INDEX [IX_Categories_Name] ON [dbo].[Categories] ([Name])
