﻿INSERT INTO [dbo].[Categories](Name, Description)
VALUES 
	('Steel String Guitar','The steel string acoustic guitar is a classic, not classical, but classic guitar of all time!'),
	('Solid Body Electric Guitars', 'This is by far the most common type of electric guitar sold and played because it’s the most versatile of them all.'),
	('Electric Bass Guitar', 'When most people opt for a bass guitar, it’s typically going to be an electric one.')
