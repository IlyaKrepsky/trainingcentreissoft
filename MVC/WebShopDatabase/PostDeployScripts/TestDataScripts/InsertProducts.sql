﻿INSERT INTO [dbo].[Products] (Name, Description, Cost, Quantity, CategoryId, ManufacturerId)
VALUES
	('Gibson Les Paul Faded 2018, Worn Bourbon','The benefits of a modern Gibson guitar and the appeal of a pawn shop prize — both are yours with the Gibson Les Paul Faded' , 700, 5, 2, 1 ),
	('Gibson SG Standard 2018, Autumn Shade', 'The new SG Standard guitar captures the essence of the groundbreaking SG of 1961 with the sonic characteristics that made this model a legend.', 950, 3, 2, 1),
	('Fender Mustang MN, Black', 'The Fender Mustang Electric Guitar, Black combines the classic offset design with modern improvements to deliver an incredibly powerful instrument', 400, 7, 2, 3),
	('Fender American Special Telecaster, Vintage Blonde', 'The Fender American Special Telecaster in Vintage Blonde is styled on classic 50s Telecaster models and provides a vintage sound and feel', 835, 1, 2, 3),
	('Fender CD-60CE NAT', 'Fender Classic Design guitars are exactly that: Classic acoustic designs with a newly designed dreadnought body shape, and other unique Fender designed features incorporating innovative acoustic design', 270, 6, 1, 3),
	('Yamaha F-310','he Yamaha F310 6-String Acoustic Guitar. Bright, sweet tone, easy play and great value.', 150, 10, 1, 4),
	('Gibson EB 4 Bass 2017', 'Gibson is adding to their iconic electric bass guitar line with this EB 4 Bass 2017. Its ergonomic contours and 34" scale length make it the most extreme-looking bass in the long legacy of the EB line', 1000, 2, 3, 1),
	('Gibson Firebird Studio 2017 T','', 1300, 1, 3, 1),
	('Epiphone Toby Standard IV','', 400, 6, 3, 2),
	('Epiphone Thunderbird Pro - Natural Oil','', 550, 3, 3, 2)