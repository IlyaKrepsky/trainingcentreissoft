﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;
using Ninject;

namespace ConsoleApp1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var kernel = new StandardKernel(new NinjectService());
            kernel.Load(Assembly.GetExecutingAssembly());
            var unit = kernel.Get<IUnitOfWork>();
            Console.WriteLine(unit.ProductRepository.Count(t=>t.Id%2 != 0));
            CrudOrdrer(unit);
            Console.ReadLine();
            CrudProduct(unit);
            Console.ReadLine();
            CrudUser(unit);
            Console.ReadLine();
            CrudCategory(unit);
            Console.ReadLine();
        }

        private static void CrudOrdrer(IUnitOfWork unit)
        {
            var orders = unit.OrderRepository.GetAll();
            ShowOrders(orders);

            Console.WriteLine("Create New Order");
            var newOrder = new Order
            {
                Number = Guid.NewGuid(),
                Date = DateTime.Today,
                User = unit.UserRepository.Get(3)
            };
            unit.OrderRepository.Create(newOrder);
            unit.Commit();
            orders = unit.OrderRepository.GetAll();
            ShowOrders(orders);

            Console.WriteLine("Change order's number and date");
            var order = unit.OrderRepository.Find(o => o.Number == newOrder.Number).First();
            Console.WriteLine($"Old order: orderNumber {order.Number}, orderDate {order.Date:d}");
            order.Number = Guid.NewGuid();
            order.Date = DateTime.Now.AddYears(-1);
            unit.Commit();
            order = unit.OrderRepository.Find(o => o.Number == order.Number).First();
            Console.WriteLine($"New order: orderNumber {order.Number}, orderDate {order.Date:d}");

            Console.WriteLine("Delete Order");
            unit.OrderRepository.Delete(orders.FirstOrDefault(o => o.Number == order.Number));
            unit.Commit();
            orders = unit.OrderRepository.GetAll();
            ShowOrders(orders);
        }

        private static void CrudProduct(IUnitOfWork unit)
        {
            var products = unit.ProductRepository.GetAll();
            ShowProducts(products);

            Console.WriteLine("Create new Product");
            var guitar = new Product
            {
                Name = "Super Guitar",
                Number = Guid.NewGuid(),
                Brand = unit.BrandRepository.Get(1),
                Category = unit.CategoryRepository.Get(1),
                Cost = 100,
                Quantity = 100
            };
            Console.WriteLine($"New Product: {guitar.Number} {guitar.Name} {guitar.Cost:C}");
            unit.ProductRepository.Create(guitar);
            unit.Commit();
            products = unit.ProductRepository.GetAll();
            ShowProducts(products);

            Console.WriteLine("Change Product Name \"Super Guitar\" -> \"Super Stratocaster\"");
            guitar = unit.ProductRepository.Find(g => g.Number == guitar.Number).First();
            guitar.Name = "Super Stratocaster";
            unit.Commit();
            products = unit.ProductRepository.GetAll();
            ShowProducts(products);

            Console.WriteLine("Delete Product");
            unit.ProductRepository.Delete(guitar);
            unit.Commit();
            products = unit.ProductRepository.GetAll();
            ShowProducts(products);

            Console.WriteLine("Take 5 product, skip 3");
            products = unit.ProductRepository.GetPage(3, 5);
            ShowProducts(products);

            Console.WriteLine("Take 2 product, skip 1 where productName contains \"Fender\"");
            products = unit.ProductRepository.GetPage(1, 2, p => p.Name.Contains("Fender"));
            ShowProducts(products);
        }

        private static void CrudUser(IUnitOfWork unit)
        {
            var users = unit.UserRepository.GetAll();
            ShowUsers(users);

            Console.WriteLine("Create new User");
            var user = new User
            {
                Email = "email@email.com",
                UserName = "SuperUser",
                RealName = "Super User",
                Password = new byte[]{},
                Phone = "1234567"
            };
            Console.WriteLine("New User:");
            Console.WriteLine($"{user.RealName} {user.UserName} {user.Email} {user.Password}");
            unit.UserRepository.Create(user);
            unit.Commit();
            users = unit.UserRepository.GetAll();
            ShowUsers(users);
            Console.WriteLine("Add Role");
            user = unit.UserRepository.Find(u => u.Email == user.Email).First();
            user.Roles.Add(unit.RoleRepository.GetAll().Last());
            unit.Commit();
            users = unit.UserRepository.GetAll();
            ShowUsers(users);
            Console.WriteLine("Delete User");
            unit.UserRepository.Delete(user);
            unit.Commit();
            users = unit.UserRepository.GetAll();
            ShowUsers(users);
        }

        private static void CrudCategory(IUnitOfWork unit)
        {
            var categories = unit.CategoryRepository.GetAll();
            ShowCategories(categories);
            Console.WriteLine("Create new Category");
            var category = new Category
            {
                Name = "Classic Guitar"
            };
            unit.CategoryRepository.Create(category);
            unit.Commit();
            categories = unit.CategoryRepository.GetAll();
            ShowCategories(categories);
            Console.WriteLine("Add Product to category");
            var oldCategoryId = 1;
            var guitar = unit.ProductRepository.Find(p => p.Category.Id == oldCategoryId).First();
            category = unit.CategoryRepository.Find(c => c.Name == category.Name).FirstOrDefault();
            category.Products.Add(guitar);
            unit.Commit();
            categories = unit.CategoryRepository.GetAll();
            ShowCategories(categories);
            category = unit.CategoryRepository.Get(oldCategoryId);
            category.Products.Add(guitar);
            unit.Commit();
            Console.WriteLine("Delete Category");
            category = unit.CategoryRepository.Find(c => c.Products.Count == 0).First();
            unit.CategoryRepository.Delete(category);
            unit.Commit();
            categories = unit.CategoryRepository.GetAll();
            ShowCategories(categories);
        }

        private static void ShowCategories(IEnumerable<Category> categories)
        {
            Console.WriteLine("Show Categories");
            foreach (var category in categories)
            {
                Console.WriteLine($"{category.Name} productsCount {category.Products.Count}");
                foreach (var prod in category.Products)
                {
                    Console.WriteLine($"\t{prod.Name}");
                }
            }

            Console.WriteLine();
        }

        private static void ShowUsers(IEnumerable<User> users)
        {
            Console.WriteLine("Show Users");
            foreach (var user in users)
            {
                Console.WriteLine($"{user.RealName} {user.UserName} {user.Email} {user.Password}");
                foreach (var role in user.Roles)
                {
                    Console.WriteLine($"\t{role.Name}");
                }
            }

            Console.WriteLine();
        }

        private static void ShowProducts(IEnumerable<Product> products)
        {
            Console.WriteLine("Show product" + Environment.NewLine);
            foreach (var item in products)
            {
                Console.WriteLine($"\t {item.Number} {item.Name}  {item.Cost:C}  {item.Quantity}");
            }

            Console.WriteLine();
        }

        private static void ShowOrders(IEnumerable<Order> orders)
        {
            Console.WriteLine("Show orders" + Environment.NewLine);
            foreach (var item in orders)
            {
                Console.WriteLine($"{item.Number} {item.Date:d}");
                foreach (var itemOrderItem in item.OrderItems)
                {
                    Console.WriteLine(
                        $"\t {itemOrderItem.Product.Name}  {itemOrderItem.Product.Cost}  {itemOrderItem.ProductCount}");
                }
            }

            Console.WriteLine();
        }
    }
}