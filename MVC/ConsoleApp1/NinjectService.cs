﻿using DAL.Context.Repositories;
using DAL.Interfaces.Interfaces;
using Ninject.Modules;

namespace ConsoleApp1
{
    internal class NinjectService : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument("WebShopConnection");
        }
    }
}