﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DAL.Entities.Entities;

namespace DAL.Interfaces.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        T Get(Expression<Func<T, bool>> predicate);
        void Create(T item);
        void Delete(T item);
        IEnumerable<T> GetPage(int skip, int take, Expression<Func<T, bool>> predicate = null);
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        int Count(Expression<Func<T, bool>> predicate = null);
    }
}