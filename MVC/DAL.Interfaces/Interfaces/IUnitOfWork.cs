﻿using System;
using DAL.Entities.Entities;

namespace DAL.Interfaces.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Order> OrderRepository { get; }
        IRepository<User> UserRepository { get; }
        IRepository<Role> RoleRepository { get; }
        IRepository<Product> ProductRepository { get; }
        IRepository<Category> CategoryRepository { get; }
        IRepository<Brand> BrandRepository { get; }
        IRepository<OrderItem> OrderItemRepository { get; }
        IRepository<Image> PictureRepository { get; }
        void Commit();
    }
}