﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebShop
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Product",
                "Guitar/Details/{number}",
                new
                {
                    controller = "Product",
                    action = "Details"
                },
                new {number = @"[({]?[a-zA-Z0-9]{8}[-]?([a-zA-Z0-9]{4}[-]?){3}[a-zA-Z0-9]{12}[})]?"}
            );

            routes.MapRoute(
                "EditProduct",
                "Guitar/Edit/{number}",
                new
                {
                    controller = "Product",
                    action = "Edit"
                },
                new {number = @"[({]?[a-zA-Z0-9]{8}[-]?([a-zA-Z0-9]{4}[-]?){3}[a-zA-Z0-9]{12}[})]?"}
            );

            routes.MapRoute(
                "ProductDelete",
                "Guitar/Delete/{number}",
                new
                {
                    controller = "Product",
                    action = "Delete"
                },
                new {number = @"[({]?[a-zA-Z0-9]{8}[-]?([a-zA-Z0-9]{4}[-]?){3}[a-zA-Z0-9]{12}[})]?"}
            );

            routes.MapRoute(
                "ImageDelete",
                "Image/Delete/{number}",
                new
                {
                    controller = "Image",
                    action = "Delete"
                },
                new {number = @"[({]?[a-zA-Z0-9]{8}[-]?([a-zA-Z0-9]{4}[-]?){3}[a-zA-Z0-9]{12}[})]?"}
            );

            routes.MapRoute(
                "UserDetails",
                "User/{name}",
                new
                {
                    controller = "Admin",
                    action = "Details"
                },
                new { name = @"\w+" }
            );

            routes.MapRoute(
                "UserDelete",
                "User/Delete/{name}",
                new
                {
                    controller = "Admin",
                    action = "Delete"
                },
                new {name = @"\w+"}
            );

            routes.MapRoute(
                "AddProduct",
                "Guitar/Add",
                new
                {
                    controller = "Product",
                    action = "Create"
                }
            );

            routes.MapRoute(
                "UserAdd",
                "User/Add",
                new
                {
                    controller = "User",
                    action = "Create"
                }
            );

            routes.MapRoute(
                "CategoryAdd",
                "Category/Add",
                new
                {
                    controller = "Category",
                    action = "Create"
                }
            );

            routes.MapRoute(
                "BrandAdd",
                "Brand/Add",
                new
                {
                    controller = "Brand",
                    action = "Create"
                }
            );

            routes.MapRoute(
                "Catalog",
                "Catalog",
                new
                {
                    controller = "Product",
                    action = "List"
                }
            );

            routes.MapRoute(
                "Admin",
                "Admin",
                new
                {
                    controller = "Admin",
                    action = "Index"
                }
            );

            routes.MapRoute(
                "Cart",
                "Cart",
                new
                {
                    controller = "Cart",
                    action = "Index"
                }
            );

            routes.MapRoute(
                "CategoryAndBrands",
                "CategoryAndBrands",
                new
                {
                    controller = "Category",
                    action = "Common"
                }
            );

            routes.MapRoute(
                "PersonalAccount",
                "PersonalAccount",
                new
                {
                    controller = "Account",
                    action = "PersonalAccount"
                }
            );

            routes.MapRoute(
                "EditAccount",
                "EditAccount",
                new
                {
                    controller = "Account",
                    action = "Edit"
                }
            );
            routes.MapRoute(
                "api",
                "api",
                new
                {
                    controller = "Common",
                    action = "Index"
                }
            );

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}