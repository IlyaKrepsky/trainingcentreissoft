﻿using System.ComponentModel;
using System.Web.Mvc;
using BLL.Models.Models;
using FluentValidation.Attributes;
using WebShop.Models.FluentValidation;

namespace WebShop.Models
{
    [Validator(typeof(CategoryValidation))]
    public class CategoryVM
    {
        private readonly CategoryDTO _category;

        [HiddenInput]
        public int Id
        {
            get => _category.Id;
            set => _category.Id = value;
        }

        [DisplayName("Category")]
        public string Name
        {
            get => _category.Name;
            set => _category.Name = value;
        }

        public string Description
        {
            get => _category.Description;
            set => _category.Description = value;
        }

        public CategoryDTO Category => _category;

        public CategoryVM(CategoryDTO brand)
        {
            _category = brand;
        }

        public CategoryVM()
        {
            _category = new CategoryDTO();
        }
    }
}