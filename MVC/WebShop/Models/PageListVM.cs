﻿using System;
using System.Collections.Generic;

namespace WebShop.Models
{
    public class PageListVM<T> : List<T>
    {
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }

        public PageListVM(IEnumerable<T> items, int current, int pageSize, int totalCount)
        {
            this.AddRange(items);
            TotalPages = (int)Math.Ceiling(totalCount / (double)pageSize);
            CurrentPage = current;
        }

    }
}