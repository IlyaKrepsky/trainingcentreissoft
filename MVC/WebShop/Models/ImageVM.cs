﻿using System;
using BLL.Models.Models;

namespace WebShop.Models
{
    public class ImageVM
    {
        public string DataUrl { get; set;}
        public string Name { get; set;}
        public string Product { get; set; }

        public ImageVM(ImageDTO image)
        {
            Name = image.Name.ToString();
            Product = image.ProductNumber.ToString();
            DataUrl = $"data:{image.MimeType};base64,{Convert.ToBase64String(image.Data)}";
        }
        public ImageVM()
        {
        }
    }
}