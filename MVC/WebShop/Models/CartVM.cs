﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;

namespace WebShop.Models
{
    public class CartVM
    {
        public Dictionary<string, OrderItemDTO> CartItems;
        public decimal GetTotal => CartItems.Values.Sum(i => i.Product.Cost * i.Count);
        public IEnumerable<OrderItemDTO> GetItems => CartItems.Values;

        public CartVM()
        {
            CartItems = new Dictionary<string, OrderItemDTO>();
        }
    }
}