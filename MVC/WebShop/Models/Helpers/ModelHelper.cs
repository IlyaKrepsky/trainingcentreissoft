﻿using System;
using BLL.Models.Models;

namespace WebShop.Models.Helpers
{
    public static class ModelHelper
    {
        public static void AddItemToCart(this CartVM cart, ProductDTO product, int quantity)
        {
            if (product.Quantity < quantity)
            {
                throw new Exception("Quantity should be less then product quantity");
            }

            if (cart.CartItems.ContainsKey(product.Number.ToString()))
            {
                var sum = cart.CartItems[product.Number.ToString()].Count + quantity;
                if (sum > product.Quantity)
                {
                    throw new Exception("Sum quantity should be less then product quantity");
                }

                cart.CartItems[product.Number.ToString()].Count += quantity;
            }
            else
            {
                cart.CartItems.Add(product.Number.ToString(), new OrderItemDTO
                {
                    Count = quantity,
                    Product = product
                });
            }
        }

        public static ImageDTO GetImageDto(this ImageVM model)
        {
            var data = model.DataUrl.Substring(model.DataUrl.IndexOf(',') + 1);
            var startIndex = model.DataUrl.IndexOf(':');
            var endIndex = model.DataUrl.IndexOf(';');
            var mime = model.DataUrl.Substring(startIndex + 1, endIndex - startIndex - 1);
            return new ImageDTO
            {
                ProductNumber = Guid.Parse(model.Product),
                Name = Guid.NewGuid(),
                MimeType = mime,
                Data = Convert.FromBase64String(data)
            };
        }

        public static UserDTO GetUserDTO(this UserVM model, UserDTO user)
        {
            user.UserName = model.UserName;
            user.RealName = model.RealName;
            user.Email = model.Email;
            user.Phone = model.Phone;
            return user;
        }
    }
}