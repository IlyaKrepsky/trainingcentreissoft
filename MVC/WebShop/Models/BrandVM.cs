﻿using System.ComponentModel;
using System.Web.Mvc;
using BLL.Models.Models;
using FluentValidation.Attributes;
using WebShop.Models.FluentValidation;

namespace WebShop.Models
{
    [Validator(typeof(BrandValidation))]
    public class BrandVM
    {
        private readonly BrandDTO _brand;

        [HiddenInput]
        public int Id
        {
            get => _brand.Id;
            set => _brand.Id = value;
        }

        [DisplayName("Brand")]
        public string Name
        {
            get => _brand.Name;
            set => _brand.Name = value;
        }

        public string Description
        {
            get => _brand.Description;
            set => _brand.Description = value;
        }

        public BrandDTO Brand => _brand;

        public BrandVM(BrandDTO brand)
        {
            _brand = brand;
        }

        public BrandVM()
        {
            _brand = new BrandDTO();
        }
    }
}