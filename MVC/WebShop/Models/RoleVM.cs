﻿namespace WebShop.Models
{
    public class RoleVM
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public bool IsChecked { get; set; }
    }
}