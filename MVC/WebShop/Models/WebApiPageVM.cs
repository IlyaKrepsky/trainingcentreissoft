﻿using System.Collections.Generic;
using BLL.Models.Models;

namespace WebShop.Models
{
    public class WebApiPageVM
    {
        public IEnumerable<ProductDTO> ProductList { get; set; }
        public int TotalPage { get; set; }
        public int CurrentPage { get; set; }
    }
}