﻿using FluentValidation;

namespace WebShop.Models.FluentValidation
{
    public class UserValidation : AbstractValidator<UserVM>
    {
        public UserValidation()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("Field is required")
                .MaximumLength(100)
                .WithMessage("User name must be less than 100 characters");
            RuleFor(x => x.RealName).NotEmpty().WithMessage("Field is required")
                .MaximumLength(200)
                .WithMessage("Real name must be less than 200 characters");
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Filed is required")
                .EmailAddress()
                .WithMessage("Invalid email format.");
            RuleFor(x => x.Phone).NotEmpty().WithMessage("Field is required")
                .Matches(@"^\+375\d{9}$");
        }
    }
}