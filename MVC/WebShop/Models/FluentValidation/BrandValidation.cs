﻿using FluentValidation;

namespace WebShop.Models.FluentValidation
{
    public class BrandValidation : AbstractValidator<BrandVM>
    {
        public BrandValidation()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Brand's name is required")
                .MaximumLength(200)
                .WithMessage("Brand's name must be less than 200 characters");
            RuleFor(x => x.Description).MaximumLength(1000)
                .WithMessage("Description must be less than 1000 characters");
        }
    }
}