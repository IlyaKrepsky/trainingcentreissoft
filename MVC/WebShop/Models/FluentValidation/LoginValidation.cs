﻿using FluentValidation;

namespace WebShop.Models.FluentValidation
{
    public class LoginValidation : AbstractValidator<LoginVM>
    {
        public LoginValidation()
        {
            RuleFor(x => x.Login).NotEmpty().WithMessage("Field is required")
                .MaximumLength(100)
                .WithMessage("Login must be less than 100 characters");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Filed is required")
                .MaximumLength(20)
                .WithMessage("Password must be less than 20 characters")
                .MinimumLength(6)
                .WithMessage("Password must be less than 20 characters");
        }
    }
}