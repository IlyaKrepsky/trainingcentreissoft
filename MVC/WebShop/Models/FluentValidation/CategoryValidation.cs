﻿using FluentValidation;

namespace WebShop.Models.FluentValidation
{
    public class CategoryValidation : AbstractValidator<CategoryVM>
    {
        public CategoryValidation()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Category's name is required")
                .MaximumLength(200)
                .WithMessage("Category's name must be less than 200 characters");
            RuleFor(x => x.Description).MaximumLength(1000)
                .WithMessage("Description must be less than 1000 characters");
        }
    }
}