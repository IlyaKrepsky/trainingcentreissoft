﻿using FluentValidation;

namespace WebShop.Models.FluentValidation
{
    public class RegisterValidation : AbstractValidator<RegisterVM>
    {
        public RegisterValidation()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("Field is required")
                .MaximumLength(100)
                .WithMessage("User name must be less than 100 characters");
            RuleFor(x => x.RealName).NotEmpty().WithMessage("Field is required")
                .MaximumLength(200)
                .WithMessage("Real name must be less than 200 characters");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Filed is required")
                .MaximumLength(20)
                .WithMessage("Password must be less than 20 characters")
                .MinimumLength(6)
                .WithMessage("Password must be less than 20 characters");
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Filed is required")
                .EmailAddress()
                .WithMessage("Invalid email format.");
            RuleFor(x => x.ConfirmEmail)
                .NotEmpty()
                .WithMessage("Filed is required")
                .EmailAddress()
                .WithMessage("Invalid email format.")
                .Equal(x=>x.Email)
                .WithMessage("Emails do not match");
            RuleFor(x => x.ConfirmPassword)
                .NotEmpty()
                .WithMessage("Filed is required")
                .Equal(x => x.Password)
                .WithMessage("Passwords do not match");
            RuleFor(x => x.Phone).NotEmpty().WithMessage("Field is required")
                .Matches(@"^\+375\d{9}$");
        }
    }
}