﻿using FluentValidation;

namespace WebShop.Models.FluentValidation
{
    public class ProductValidation : AbstractValidator<ProductVM>
    {
        public ProductValidation()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Product's name is required")
                .MaximumLength(200)
                .WithMessage("Product's name must be less than 200 characters");
            RuleFor(x => x.Description).MaximumLength(1000)
                .WithMessage("Description must be less than 1000 characters");
            RuleFor(x => x.Quantity).NotEmpty().WithMessage("Quantity is required")
                .InclusiveBetween(0, int.MaxValue)
                .WithMessage($"Quantity must be more then 0 and less then {int.MaxValue}");
            RuleFor(x => x.Cost).NotEmpty().WithMessage("Cost is required").InclusiveBetween(1, 100000)
                .WithMessage("Cost must be more then 0 and less then 100000");
        }
    }
}