﻿using System.ComponentModel;
using BLL.Models.Models;
using FluentValidation.Attributes;
using WebShop.Models.FluentValidation;

namespace WebShop.Models
{
    [Validator(typeof(UserValidation))]
    public class UserVM
    {
        public string Name { get; set; }

        [DisplayName("Login")]
        public string UserName { get; set; }

        [DisplayName("Name")]
        public string RealName { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Phone")]
        public string Phone { get; set; }

        public UserVM(UserDTO user)
        {
            Name = user.UserName;
            UserName = user.UserName;
            RealName = user.RealName;
            Email = user.Email;
            Phone = user.Phone;
        }

        public UserVM()
        {
        }
    }
}