﻿using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using WebShop.Models.FluentValidation;


namespace WebShop.Models
{
    [Validator(typeof(LoginValidation))]
    public class LoginVM
    {
        public string Login {get; set;}

        [DataType(DataType.Password)]
        public string Password {get; set;}
    }
}