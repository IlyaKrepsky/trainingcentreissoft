﻿using System;
using System.ComponentModel.DataAnnotations;
using BLL.Models.Models;
using FluentValidation.Attributes;
using WebShop.Models.FluentValidation;

namespace WebShop.Models
{
    [Validator(typeof(ProductValidation))]
    public class ProductVM
    {
        private readonly ProductDTO _product;

        public string Number
        {
            get => _product.Number.ToString();
            set => _product.Number = value != null? Guid.Parse(value): Guid.NewGuid();
        }

        public string Name
        {
            get => _product.Name;
            set => _product.Name = value;
        }

        public string Description
        {
            get => _product.Description;
            set => _product.Description = value;
        }

        public int Quantity
        {
            get => _product.Quantity;
            set => _product.Quantity = value;
        }

        [Display(Name = "Price")]
        public decimal Cost
        {
            get => _product.Cost;
            set => _product.Cost = value;
        }

        [Display(Name = "Category")]
        public string CategoryName
        {
            get => _product.CategoryName;
            set => _product.CategoryName = value;
        }

        [Display(Name = "Brand")]
        public string BrandName
        {
            get => _product.BrandName;
            set => _product.BrandName = value;
        }

        public ProductDTO Product => _product;

        public ProductVM()
        {
            _product = new ProductDTO();
        }

        public ProductVM(ProductDTO product)
        {
            _product = product;
        }
    }
}