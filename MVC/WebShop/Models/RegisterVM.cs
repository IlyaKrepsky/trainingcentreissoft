﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BLL.Models.Models;
using FluentValidation.Attributes;
using WebShop.Models.FluentValidation;

namespace WebShop.Models
{
    [Validator(typeof(RegisterValidation))]
    public class RegisterVM
    {
        [DisplayName("Login")]
        public string UserName
        {
            get => User.UserName;
            set => User.UserName = value;
        }

        [DisplayName("Name")]
        public string RealName
        {
            get => User.RealName;
            set => User.RealName = value;
        }

        [DisplayName("E-mail")]
        public string Email
        {
            get => User.Email;
            set => User.Email = value;
        }

        [DisplayName("Confirm e-mail")] public string ConfirmEmail { get; set; }

        [DisplayName("Phone")]
        public string Phone
        {
            get => User.Phone;
            set => User.Phone = value;
        }

        [DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password
        {
            get => User.Password;
            set => User.Password = value;
        }

        [DisplayName("Confirm password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public UserDTO User { get; }

        public RegisterVM(UserDTO user)
        {
            User = user;
            ConfirmEmail = User.Email;
            ConfirmPassword = User.Password;
        }

        public RegisterVM()
        {
            User = new UserDTO();
        }
    }
}