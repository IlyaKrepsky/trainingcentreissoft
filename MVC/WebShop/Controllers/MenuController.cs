﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class MenuController : Controller
    {
        private readonly List<MenuItemsVM> _menuItems;

        public MenuController()
        {
            _menuItems = new List<MenuItemsVM>
            {
                new MenuItemsVM {Name = "Home", Controller = "Home", Action = "Index", Active = string.Empty},
                new MenuItemsVM {Name = "Catalog", Controller = "Product", Action = "List", Active = string.Empty},
                new MenuItemsVM
                    {Name = "Categories & Brands", Controller = "Category", Action = "Common", Active = string.Empty},
                new MenuItemsVM
                    {Name = "Api", Controller = "Common", Action = "Index", Active = string.Empty},
            };
        }

        public ActionResult Index(string action = "Index", string controller = "Home")
        {
            if (User.IsInRole("admin"))
            {
                _menuItems.Add(new MenuItemsVM { Name = "Admin", Controller = "Admin", Action = "Index", Active = string.Empty });
            }
            var item = _menuItems.FirstOrDefault(g => g.Controller == controller);
            if (item != null)
            {
                item.Active = "active";
            }
            return PartialView(_menuItems);
        }
    }
}