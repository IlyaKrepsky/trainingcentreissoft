﻿using System;
using System.Web.Mvc;
using Interfaces.Interfaces;
using WebShop.Filters;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }

        public ActionResult Common()
        {
            return View();
        }

        public ActionResult Index()
        {
            var category = _service.GetAll();
            return PartialView(category);
        }

        public ActionResult Details(int id)
        {
            var category = _service.Get(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return PartialView(category);
        }

        [UserAuthorize(Roles = "admin")]
        public ActionResult Create()
        {
            return PartialView(new CategoryVM());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorize(Roles = "admin")]
        public ActionResult Create(CategoryVM category)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Create", category);
            }
            try
            {
                _service.Create(category.Category);
                return RedirectToAction("Index", "Common");
            }
            catch
            {
                return RedirectToAction("Index", "Common");
            }
        }

        [UserAuthorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            var category = _service.Get(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return PartialView(new CategoryVM(category));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorize(Roles = "admin")]
        public ActionResult Edit(CategoryVM category)
        {
            try
            {
                _service.Update(category.Category);
                return RedirectToAction("Index", "Common");
            }
            catch
            {
                return RedirectToAction("Index", "Common");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorize(Roles = "admin")]
        public JsonResult Delete(int id)
        {
            try
            {
                _service.Delete(id);
                return Json(new {success = true, data = "You deleted the item"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, data = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }
    }
}