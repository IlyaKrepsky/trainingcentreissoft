﻿using System.Web.Mvc;

namespace WebShop.Controllers
{
    public class CommonController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}