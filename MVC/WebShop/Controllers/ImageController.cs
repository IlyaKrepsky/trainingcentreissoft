﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BLL.Models.Models;
using Interfaces.Interfaces;
using WebShop.Filters;
using WebShop.Models;
using WebShop.Models.Helpers;

namespace WebShop.Controllers
{
    public class ImageController : Controller
    {
        private readonly IImageService _service;

        public ImageController(IImageService service)
        {
            _service = service;
        }

        public ActionResult Index(string product)
        {
            var images = new List<ImageVM>();
            images = GetProductsImages(Guid.Parse(product)).ToList();
            return View(images);
        }

        public ActionResult ProductCardImage(string product)
        {
            var image = new ImageDTO();
            image = _service.Get(Guid.Parse(product));
            return PartialView(image != null ? new ImageVM(image) : new ImageVM());
        }

        [UserAuthorize(Roles = "admin")]
        public PartialViewResult GetImages(string product)
        {
            var images = GetProductsImages(Guid.Parse(product)).ToList();
            return PartialView(images);
        }

        [HttpPost]
        [UserAuthorize(Roles = "admin")]
        public ActionResult CreateImage(ImageVM imageVm)
        {
            var image = imageVm.GetImageDto();
            try
            {
                _service.Create(image);
                return Json(new {success = true, data = $"{image.Name.ToString()}"},
                    JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new {success = false},
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [UserAuthorize(Roles = "admin")]
        public JsonResult Delete(string number)
        {
            try
            {
                _service.Delete(Guid.Parse(number));
                return Json(new {success = true, responseText = $"{number}"}, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new {success = false, responseText = $"You could not remove the image № {number}"},
                    JsonRequestBehavior.AllowGet);
            }
        }

        private IEnumerable<ImageVM> GetProductsImages(Guid product)
        {
            var images = _service.Find(product);
            return images.Select(img => new ImageVM(img)).ToList();
        }
    }
}