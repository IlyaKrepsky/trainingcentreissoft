﻿using System;
using System.Linq;
using System.Web.Mvc;
using Interfaces.Interfaces;
using WebShop.Filters;
using WebShop.Models;
using WebShop.Models.Helpers;

namespace WebShop.Controllers
{
    public class CartController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IProductService _service;

        public CartController(IProductService service, IOrderService orderService)
        {
            _service = service;
            _orderService = orderService;
        }

        [UserAuthorize(Roles = "user")]
        public ActionResult Index()
        {
            var cart = GetCart().CartItems.Values;
            return View(cart);
        }

        public ActionResult Cart()
        {
            return PartialView(GetCart());
        }

        [HttpPost]
        [UserAuthorize(Roles = "user")]
        public ActionResult AddToCart(string number, int quantity = 1)
        {
            try
            {
                var product = _service.Get(Guid.Parse(number));
                if (product != null)
                {
                    GetCart().AddItemToCart(product, quantity);
                }

                return PartialView("Cart", GetCart());
            }
            catch (Exception e)
            {
                return Json(new {success = false, data = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        [UserAuthorize(Roles = "user, admin")]
        public ActionResult UserOrder(string name)
        {
            var orders = _orderService.Find(name);
            var sum = orders.Sum(order => order.OrderItems.Sum(item => item.Count * item.Product.Cost));
            ViewBag.Sum = $"Sum: {sum:C}";
            return PartialView(orders);
        }

        [HttpPost]
        [UserAuthorize(Roles = "user")]
        public JsonResult MakeOrder()
        {
            try
            {
                var cart = GetCart().GetItems.ToList();
                var orderNumber = Guid.NewGuid();
                var user = User.Identity.Name;
                cart.ForEach(c => c.OrderNumber = orderNumber);
                _orderService.Create(user, cart);
                Session[user] = null;
                return Json(new {success = true, data = ""}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, data = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [UserAuthorize(Roles = "user")]
        public JsonResult Delete(string key)
        {
            var cart = GetCart();
            try
            {
                if (cart.CartItems.ContainsKey(key))
                {
                    cart.CartItems.Remove(key);
                }

                return Json(new {success = true, data = "You deleted the item"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, data = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        private CartVM GetCart()
        {
            if (!(Session[User.Identity.Name] is CartVM cart))
            {
                cart = new CartVM();
                Session[User.Identity.Name] = cart;
            }
            return cart;
        }
    }
}