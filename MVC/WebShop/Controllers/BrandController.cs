﻿using System;
using System.Web.Mvc;
using Interfaces.Interfaces;
using WebShop.Filters;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService _service;

        public BrandController(IBrandService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var brands = _service.GetAll();
            return PartialView(brands);
        }

        public ActionResult Details(int id)
        {
            var brand = _service.Get(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return PartialView(brand);
        }

        [UserAuthorize(Roles = "admin")]
        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorize(Roles = "admin")]
        public ActionResult Create(BrandVM brand)
        {
            if (ModelState.IsValid)
            {
                return PartialView("Create", brand);
            }

            try
            {
                _service.Create(brand.Brand);
                return RedirectToAction("Index", "Common");
            }
            catch
            {
                return RedirectToAction("Index", "Common");
            }
        }

        [UserAuthorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            var brand = _service.Get(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return PartialView(new BrandVM(brand));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorize(Roles = "admin")]
        public ActionResult Edit(BrandVM brand)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Edit", brand);
            }

            try
            {
                _service.Update(brand.Brand);
                return RedirectToAction("Index", "Common");
            }
            catch
            {
                return RedirectToAction("Index", "Common");
            }
        }

        [HttpPost]
        [UserAuthorize(Roles = "admin")]
        public JsonResult Delete(int id)
        {
            try
            {
                _service.Delete(id);
                return Json(new {success = true, data = "You deleted the item"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, data = e.Message}, JsonRequestBehavior.AllowGet);
            }
        }
    }
}