﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using BLL.Models.Models;
using Interfaces.Interfaces;
using WebShop.Authentication;
using WebShop.Filters;
using WebShop.Models;
using WebShop.Models.Helpers;

namespace WebShop.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _service;

        public AccountController(IUserService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = new UserDTO();
            try
            {
                user = _service.Login(model.Login, model.Password);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                return View();
            }

            Response.SetAuthCookie(user);
            return RedirectToAction("Index", "Home");
        }


        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new RegisterVM());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterVM model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var user = model.User;
                _service.Create(user);
                user = _service.Get(user.UserName);
                Response.SetAuthCookie(user);
                return RedirectToAction("Index", "Home");
            }
            catch (ArgumentException e)
            {
                ModelState.AddModelError(string.Empty, e.Message);
                return View(model);
            }
        }

        [UserAuthorize]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [UserAuthorize]
        public ActionResult Edit()
        {
            var name = User.Identity.Name;
            var user = _service.Get(name);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(new UserVM(user));
        }

        [HttpPost]
        [UserAuthorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserVM userVm)
        {
            if (!ModelState.IsValid)
            {
                return View(userVm);
            }

            try
            {
                var user = _service.Get(userVm.Name);
                user = userVm.GetUserDTO(user);
                _service.Update(userVm.Name, user);
                FormsAuthentication.SignOut();
                Response.SetAuthCookie(user);
                return RedirectToAction("PersonalAccount");
            }
            catch (ArgumentException e)
            {
                ModelState.AddModelError("", e.Message);
                return View();
            }
        }

        [UserAuthorize]
        public ActionResult PersonalAccount()
        {
            var name = User.Identity.Name;
            var user = _service.Get(name);
            return View(user);
        }
    }
}