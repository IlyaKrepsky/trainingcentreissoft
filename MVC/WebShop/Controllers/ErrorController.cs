﻿using System.Web.Mvc;

namespace WebShop.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult Index()
        {
            Response.StatusCode = 500;
            return View();
        }

        public ViewResult AccessDenied()
        {
            Response.StatusCode = 403;
            return View();
        }

        public ViewResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }
    }
}