﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BLL.Models.Models;
using Interfaces.Interfaces;
using WebShop.Filters;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class ProductController : Controller
    {
        private readonly IBrandService _brandService;
        private readonly ICategoryService _categoryService;
        private readonly int _pageSize = 3;
        private readonly IProductService _service;

        public ProductController(IProductService service, ICategoryService categoryService, IBrandService brandService)
        {
            _service = service;
            _categoryService = categoryService;
            _brandService = brandService;
        }

        public ActionResult List(string category = null, string brand = null, int page = 1)
        {
            var items = _service.GetPage((page - 1) * _pageSize, _pageSize, category, brand);
            var totalCount = _service.Count(category, brand);
            var model = new PageListVM<ProductDTO>(items, page, _pageSize, totalCount);
            if (Request.IsAjaxRequest())
            {
                return PartialView("ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Details(string number)
        {
            var product = _service.Get(Guid.Parse(number));
            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        public PartialViewResult SelectCategory()
        {
            var category = _categoryService.GetAll().OrderBy(c => c.Name).Select(c => c.Name).ToList();
            return PartialView(category);
        }

        public PartialViewResult SelectBrand()
        {
            var category = _brandService.GetAll().OrderBy(c => c.Name).Select(c => c.Name).ToList();
            return PartialView(category);
        }

        [UserAuthorize(Roles = "admin")]
        public ActionResult Create()
        {
            ViewBag.Categories = CategoriesSelect();
            ViewBag.Brands = BrandsSelect();
            return View();
        }

        [HttpPost]
        [UserAuthorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductVM productVM)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categories = CategoriesSelect();
                ViewBag.Brands = BrandsSelect();
                return View(productVM);
            }

            var product = productVM.Product;
            _service.Create(product);
            return RedirectToAction("List");
        }

        [UserAuthorize(Roles = "admin")]
        public ActionResult Edit(string number)
        {
            ViewBag.Categories = CategoriesSelect();
            ViewBag.Brands = BrandsSelect();
            var product = _service.Get(Guid.Parse(number));
            if (product == null)
            {
                return HttpNotFound();
            }

            return View(new ProductVM(product));
        }

        [HttpPost]
        [UserAuthorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductVM product)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categories = CategoriesSelect();
                ViewBag.Brands = BrandsSelect();
                return View(product);
            }

            _service.Update(product.Product);
            return RedirectToAction("List");
        }

        [HttpPost]
        [UserAuthorize(Roles = "admin")]
        public JsonResult Delete(string number)
        {
            try
            {
                _service.Delete(Guid.Parse(number));
                return Json(new {success = true, data = $"You deleted the product № {number}"},
                    JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new {success = false, data = $"You could not remove the product № {number}"},
                    JsonRequestBehavior.AllowGet);
            }
        }

        private IEnumerable<SelectListItem> CategoriesSelect()
        {
            return _categoryService.GetAll().Select(c => new SelectListItem {Text = c.Name, Value = c.Name});
        }

        private IEnumerable<SelectListItem> BrandsSelect()
        {
            return _brandService.GetAll().Select(b => new SelectListItem {Text = b.Name, Value = b.Name});
        }
    }
}