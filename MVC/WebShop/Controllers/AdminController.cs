﻿using System;
using System.Linq;
using System.Web.Mvc;
using Interfaces.Interfaces;
using WebShop.Filters;
using WebShop.Models;

namespace WebShop.Controllers
{
    [UserAuthorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly IUserService _service;

        public AdminController(IUserService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var users = _service.GetAll();
            return View(users);
        }


        public ActionResult Details(string name)
        {
            var user = _service.Get(name);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }


        public PartialViewResult UserRoles(string name)
        {
            var allRoles = _service.GetAllRoles();
            var user = _service.Get(name);
            var roles = allRoles.Select(role => new RoleVM
                {UserName = user.UserName, Role = role, IsChecked = user.Roles.Contains(role)}).ToList();
            return PartialView(roles);
        }

        [HttpPost]
        public ActionResult UserRoles(RoleVM[] roles)
        {
            _service.SaveRoles(roles.First().UserName, roles.Where(s => s.IsChecked).Select(r => r.Role));
            return PartialView(roles);
        }

        [HttpPost]
        public JsonResult Delete(string name)
        {
            if (User.Identity.Name == name)
            {
                return Json(new {success = false, data = "You can not delete yourself"},
                    JsonRequestBehavior.AllowGet);
            }

            try
            {
                _service.Delete(name);
                return Json(name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new {success = false, data = e.Message},
                    JsonRequestBehavior.AllowGet);
            }
        }
    }
}