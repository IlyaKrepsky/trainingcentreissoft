﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Interfaces.Interfaces;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class GuitarController : ApiController
    {
        private readonly IBrandService _brandService;
        private readonly ICategoryService _categoryService;
        private readonly int _pageSize = 3;

        private readonly IProductService _service;

        public GuitarController(IProductService service, ICategoryService categoryService, IBrandService brandService)
        {
            _brandService = brandService;
            _service = service;
            _categoryService = categoryService;
        }

        [HttpGet]
        [ActionName("List")]
        public IHttpActionResult GetPage(string category = null, string brand = null, int page = 1)
        {
            var items = _service.GetPage((page - 1) * _pageSize, _pageSize, category, brand);
            var totalCount = _service.Count(category, brand);
            var model = new WebApiPageVM
            {
                ProductList = items,
                CurrentPage = page,
                TotalPage = (int) Math.Ceiling(totalCount / (double) _pageSize)
            };
            if (!model.ProductList.Any())
            {
                return NotFound();
            }

            return Ok(model);
        }

        [HttpGet]
        [ActionName("Product")]
        public IHttpActionResult Get(string id)
        {
            var product = _service.Get(Guid.Parse(id));
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }


        [HttpPost]
        [ActionName("Create")]
        public IHttpActionResult Create(ProductVM product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            _service.Create(product.Product);
            return Ok();
        }

        [HttpGet]
        [ActionName("Category")]
        public IEnumerable<string> GetCategories()
        {
            return _categoryService.GetAll().Select(s => s.Name);
        }

        [HttpGet]
        [ActionName("Brand")]
        public IEnumerable<string> GetBrands()
        {
            return _brandService.GetAll().Select(s => s.Name);
        }


        [HttpDelete]
        [ActionName("Delete")]
        public void Delete(string id)
        {
            _service.Delete(Guid.Parse(id));
        }
    }
}