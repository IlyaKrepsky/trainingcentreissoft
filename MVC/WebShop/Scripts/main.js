﻿$("#blogCarousel").carousel({
    interval: 3000
});

$(document).on("click", ".add-cart-button", AddToCart);
$(document).on("click", ".ajax-page-link", changeUrl);
$(document).on("click", ".dialog-link", ShowModal);

function AddToCart(e) {
    var element = e.target.closest(".add-cart-button");
    var select = element.nextElementSibling.closest(".quantity_select");
    if (select.options[select.selectedIndex] !== undefined) {
        var quantity = select.options[select.selectedIndex].value;
        var url = element.getAttribute("data-href") + "&quantity=" + quantity;
        $.ajax({
            type: "POST",
            url: url,
            data: "",
        }).done(function(result) {
            if (result.hasOwnProperty("success") && result.success === false) {
                alert(result.data);
            } else {
                $("#cart-View").html(result);
            }
        });
    }

}

function DeleteIsComplete(result) {
    var json = result.responseJSON;
    if (json.hasOwnProperty("success") && json.success === false) {
        alert(json.data);
    } else {
        window.location = window.location.href;
    }
}

function changeUrl(e) {
    var element = e.target.closest(".ajax-page-link");
    var href = element.href;
    window.history.pushState({}, "", href);
}

function ShowModal(e) {
    e.preventDefault();
    var element = e.target.closest(".dialog-link");
    $.get(element.href,
        function(data) {
            $("#dialog-content").html(data);
            $.validator.unobtrusive.parse("#modal-form");
            $("#mod-dialog").modal("show");
        });
}