﻿using BLL.Services.Services;
using Interfaces.Interfaces;
using Ninject.Modules;

namespace WebShop.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        private readonly string _connectionString;

        public NinjectRegistrations(string connectionString)
        {
            _connectionString = connectionString;
        }

        public override void Load()
        {
            Bind<IProductService>().To<ProductService>().WithConstructorArgument(_connectionString);
            Bind<ICategoryService>().To<CategoryService>().WithConstructorArgument(_connectionString);
            Bind<IBrandService>().To<BrandService>().WithConstructorArgument(_connectionString);
            Bind<IImageService>().To<ImageService>().WithConstructorArgument(_connectionString);
            Bind<IUserService>().To<UserService>().WithConstructorArgument(_connectionString);
            Bind<IOrderService>().To<OrderService>().WithConstructorArgument(_connectionString);
        }
    }
}