﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using BLL.Services.Services;
using FluentValidation.Mvc;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using WebShop.App_Start;
using WebShop.Authentication;
using WebShop.Util;

namespace WebShop
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            FluentValidationModelValidatorProvider.Configure();

            NinjectModule registrations = new NinjectRegistrations("WebShopConnection");
            var kernel = new StandardKernel(registrations);
            GlobalConfiguration.Configuration.DependencyResolver = new Ninject.WebApi.DependencyResolver.NinjectDependencyResolver(kernel);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                var serializer = new JavaScriptSerializer();
                var serializeModel = serializer.Deserialize<SerializedAuthUser>(authTicket.UserData);
                var user = new AuthUser(new GenericIdentity(authTicket.Name), serializeModel.Roles)
                {
                    Email = serializeModel.Email
                };
                HttpContext.Current.User = user;
            }
        }
    }
}
