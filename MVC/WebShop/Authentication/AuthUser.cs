﻿using System.Security.Principal;

namespace WebShop.Authentication
{
    public class AuthUser : GenericPrincipal
    {
        public string Email { get; set; }
        public AuthUser(IIdentity identity, string[] roles) : base(identity, roles)
        {
        }
    }
}