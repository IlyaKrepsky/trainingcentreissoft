﻿using System;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using BLL.Models.Models;

namespace WebShop.Authentication
{
    public static class Authentication
    {
        public static void SetAuthCookie(this HttpResponseBase responce, UserDTO user)
        {
            var serializedUser = new SerializedAuthUser()
            {
                Email = user.Email,
                Roles = user.Roles.ToArray()
            };

            var serializer = new JavaScriptSerializer();
            var userData = serializer.Serialize(serializedUser);

            var authTicket = new FormsAuthenticationTicket(
                1,
                user.UserName,
                DateTime.Now,
                DateTime.Now.AddMinutes(60),
                false,
                userData);
            var ticket = FormsAuthentication.Encrypt(authTicket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticket);

            responce.Cookies.Add(cookie);
        }
    }
}