﻿namespace WebShop.Authentication
{
    public class SerializedAuthUser
    {
        public string Email { get; set; }
        public string[] Roles { get; set; }
    }
}