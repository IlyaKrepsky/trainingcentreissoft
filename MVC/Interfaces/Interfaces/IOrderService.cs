﻿using System.Collections.Generic;
using BLL.Models.Models;

namespace Interfaces.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<OrderDTO> Find(string name);
        void Create(string name, IEnumerable<OrderItemDTO> orderItems);
    }
}