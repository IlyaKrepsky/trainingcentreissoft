﻿using System.Collections.Generic;
using BLL.Models.Models;

namespace Interfaces.Interfaces
{
    public interface IBrandService
    {
        IEnumerable<BrandDTO> GetAll();
        BrandDTO Get(int id);
        void Create(BrandDTO category);
        void Delete(int id);
        void Update(BrandDTO category);
    }
}