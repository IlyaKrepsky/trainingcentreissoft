﻿using System;
using System.Collections.Generic;
using BLL.Models.Models;

namespace Interfaces.Interfaces
{
    public interface IProductService
    {
        IEnumerable<ProductDTO> GetAll();
        ProductDTO Get(Guid number);
        void Delete(Guid number);
        void Update(ProductDTO productDto);
        void Create(ProductDTO product);
        IEnumerable<ProductDTO> GetPage(int skip = 1, int take = 1, string category = null, string brand = null);
        int Count(string category = null, string brand = null);
    }
}