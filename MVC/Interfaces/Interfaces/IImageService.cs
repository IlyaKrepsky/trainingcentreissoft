﻿using System;
using System.Collections.Generic;
using BLL.Models.Models;

namespace Interfaces.Interfaces
{
    public interface IImageService
    {
        void Delete(Guid name);
        IEnumerable<ImageDTO> Find(Guid productNumber);
        ImageDTO Get(Guid productNumber);
        void Create(ImageDTO image);
    }
}