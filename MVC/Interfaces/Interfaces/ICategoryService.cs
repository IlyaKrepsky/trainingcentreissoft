﻿using System.Collections.Generic;
using BLL.Models.Models;

namespace Interfaces.Interfaces
{
    public interface ICategoryService
    {
        IEnumerable<CategoryDTO> GetAll();
        CategoryDTO Get(int id);
        void Create(CategoryDTO category);
        void Delete(int id);
        void Update(CategoryDTO category);
    }
}