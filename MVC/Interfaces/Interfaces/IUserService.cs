﻿using System.Collections.Generic;
using BLL.Models.Models;

namespace Interfaces.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAll();
        UserDTO Get(string name);
        void Update(string name, UserDTO user);
        void Delete(string name);
        void Create(UserDTO user);
        IEnumerable<string> GetAllRoles();
        void SaveRoles(string name, IEnumerable<string> roles);
        UserDTO Login(string name, string password);
    }
}