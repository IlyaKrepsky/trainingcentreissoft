﻿using System;

namespace DAL.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}