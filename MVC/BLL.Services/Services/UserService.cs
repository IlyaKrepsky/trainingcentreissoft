﻿using BLL.Models.Models;
using BLL.Services.Helper;
using BLL.Services.Mappers;
using DAL.Context.Repositories;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;
using Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unit;

        public UserService(string connectionString)
        {
            _unit = new UnitOfWork(connectionString);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            var users = _unit.UserRepository.GetAll();
            return users.MapToDTOCollection();
        }

        public UserDTO Get(string name)
        {
            var user = _unit.UserRepository.Get(u => u.UserName == name);
            return user.MapToDTO();
        }

        public void Update(string name, UserDTO user)
        {
            var userEntity = _unit.UserRepository.Get(u => u.UserName == name);
            if (_unit.UserRepository.Find(u => (u.Email == user.Email || u.UserName == user.UserName)&& u.Id != userEntity.Id).Any())
            {
                throw new ArgumentException("Another user with such an email or login already exists");
            }
            user.MapToEntity(userEntity);
            _unit.Commit();
        }

        public void Delete(string name)
        {
            var user = _unit.UserRepository.Get(u => u.UserName == name);
            if (user != null)
            {
                if (user.Roles.Any(r => r.Name == "admin"))
                {
                    throw new ArgumentException("You can not remove the admin");
                }

                _unit.UserRepository.Delete(user);
            }

            _unit.Commit();
        }

        public void Create(UserDTO user)
        {
            var userEntity = _unit.UserRepository.Get(u => u.Email == user.Email || u.UserName == user.UserName);
            if (userEntity != null)
            {
                throw new ArgumentException("User with such an email or login already exists");
            }

            userEntity = user.MapToEntity(new User());
            userEntity.Roles.Add(_unit.RoleRepository.Get(r => r.Name == "user"));
            _unit.UserRepository.Create(userEntity);
            _unit.Commit();
        }

        public IEnumerable<string> GetAllRoles()
        {
            return _unit.RoleRepository.GetAll().Select(s => s.Name);
        }

        public void SaveRoles(string name, IEnumerable<string> roles)
        {
            if (!roles.Any())
            {
                return;
            }
            var user = _unit.UserRepository.Get(u => u.UserName == name);
            foreach (var roleName in roles)
            {
                var role = _unit.RoleRepository.Get(r => r.Name == roleName);
                if (!user.Roles.Contains(role))
                {
                    user.Roles.Add(role);
                }
            }
            foreach (var role in user.Roles)
            {
                if (!roles.Contains(role.Name))
                {
                    role.Users.Remove(user);
                }
            }
            _unit.Commit();
        }

        public UserDTO Login(string name, string password)
        {
            var userEntity = _unit.UserRepository.Get(u=> u.UserName == name);
            if (userEntity == null)
            {
                throw new ArgumentException("User with such login isn't exist");
            }
            if (!SecuredPassword.VerifyPassword(password, userEntity.Password))
            {
                throw new ArgumentException("Incorrect password");
            }
            return userEntity.MapToDTO();
        }

    }
}