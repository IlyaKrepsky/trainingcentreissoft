﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Mappers;
using DAL.Context.Repositories;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;
using Interfaces.Interfaces;

namespace BLL.Services.Services
{
    public class ImageService : IImageService
    {
        private readonly IUnitOfWork _unit;

        public ImageService(string connectionString)
        {
            _unit = new UnitOfWork(connectionString);
        }

        public void Delete(Guid name)
        {
            var image = _unit.PictureRepository.Get(p => p.Name == name);
            _unit.PictureRepository.Delete(image);
            _unit.Commit();
        }

        public IEnumerable<ImageDTO> Find(Guid productNumber)
        {
            var imageEntities = _unit.PictureRepository.Find(p => p.Product.Number == productNumber);
            return imageEntities.MapToDTOCollection();
        }

        public ImageDTO Get(Guid productNumber)
        {
            var imageEntity = _unit.PictureRepository.Get(p => p.Product.Number == productNumber);
            return imageEntity?.MapToDTO();
        }

        public void Create(ImageDTO image)
        {
            var entity = image.MapToEntity(new Image());
            entity.Product = _unit.ProductRepository.GetAll().First();
            entity.Product = _unit.ProductRepository.Get(p => p.Number == image.ProductNumber);
            _unit.PictureRepository.Create(entity);
            _unit.Commit();
        }
    }
}