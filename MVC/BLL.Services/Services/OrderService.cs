﻿using System;
using System.Collections.Generic;
using BLL.Models.Models;
using BLL.Services.Mappers;
using DAL.Context.Repositories;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;
using Interfaces.Interfaces;

namespace BLL.Services.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unit;

        public OrderService(string connectionString)
        {
            _unit = new UnitOfWork(connectionString);
        }

        public IEnumerable<OrderDTO> Find(string name)
        {
            var orders = _unit.OrderRepository.Find(o => o.User.UserName == name);
            return orders.MapToDTOCollection();
        }

        public void Create(string name, IEnumerable<OrderItemDTO> orderItems)
        {
            var order = new Order()
            {
                Number = Guid.NewGuid(),
                User = _unit.UserRepository.Get(u=>u.UserName == name),
                Date = DateTime.Now
            };
            foreach (var item in orderItems)
            {
                var product = _unit.ProductRepository.Get(p => p.Number == item.Product.Number);
                if (product.Quantity < item.Count)
                {
                    throw new ArgumentException($"Sorry, but there are only {product.Quantity} {product.Name} items left in stock. Delete items from the card and change the order");
                }
                product.Quantity -= item.Count;
                order.OrderItems.Add(new OrderItem
                {
                    Order = order,
                    Product = product,
                    ProductCount = item.Count
                });
            }
            _unit.OrderRepository.Create(order);
            _unit.Commit();
        }

    }
}