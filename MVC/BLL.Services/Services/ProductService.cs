﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BLL.Models.Models;
using BLL.Services.Mappers;
using DAL.Context.Repositories;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;
using Interfaces.Interfaces;

namespace BLL.Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unit;

        public ProductService(string connectionString)
        {
            _unit = new UnitOfWork(connectionString);
        }

        public IEnumerable<ProductDTO> GetAll()
        {
            var products = _unit.ProductRepository.GetAll();
            return products.MapToDTOCollection();
        }

        public void Delete(Guid number)
        {
            var product = _unit.ProductRepository.Get(p => p.Number == number);
            _unit.ProductRepository.Delete(product);
            _unit.Commit();
        }

        public void Update(ProductDTO productDto)
        {
            var product = _unit.ProductRepository.Get(p => p.Number == productDto.Number);
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product), "Product is null");
            }

            product = productDto.MapToEntity(product);
            product.Category = _unit.CategoryRepository.Get(c => c.Name == productDto.CategoryName);
            product.Brand = _unit.BrandRepository.Get(b => b.Name == productDto.BrandName);
            _unit.Commit();
        }

        public ProductDTO Get(Guid number)
        {
            var product = _unit.ProductRepository.Get(p => p.Number == number);
            return product.MapToDTO();
        }

        public IEnumerable<ProductDTO> GetPage(int skip, int take, string category, string brand)
        {
            var predicate = Predicate(category, brand);
            var productPage = _unit.ProductRepository.GetPage(skip, take, predicate);
            return productPage.MapToDTOCollection();
        }

        public int Count(string category, string brand)
        {
            var predicate = Predicate(category, brand);
            return _unit.ProductRepository.Count(predicate);
        }

        public void Create(ProductDTO product)
        {
            var productEntity = product.MapToEntity(new Product());
            productEntity.Category = _unit.CategoryRepository.Get(c => c.Name == product.CategoryName);
            productEntity.Brand = _unit.BrandRepository.Get(b => b.Name == product.BrandName);
            _unit.ProductRepository.Create(productEntity);
            _unit.Commit();
        }

        private static Expression<Func<Product, bool>> Predicate(string category, string brand)
        {
            Expression<Func<Product, bool>> predicate = null;
            if (category != null && brand != null)
            {
                return predicate = p => p.Category.Name == category && p.Brand.Name == brand;
            }

            if (brand != null)
            {
                return predicate = p => p.Brand.Name == brand;
            }

            if (category != null)
            {
                return predicate = p => p.Category.Name == category;
            }

            return predicate;
        }
    }
}