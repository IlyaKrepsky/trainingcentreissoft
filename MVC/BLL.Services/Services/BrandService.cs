﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Mappers;
using DAL.Context.Repositories;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;
using Interfaces.Interfaces;

namespace BLL.Services.Services
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWork _unit;

        public BrandService(string connectionString)
        {
            _unit = new UnitOfWork(connectionString);
        }

        public IEnumerable<BrandDTO> GetAll()
        {
            var brands = _unit.BrandRepository.GetAll();
            return brands.MapToDTOCollection();
        }

        public BrandDTO Get(int id)
        {
            var brandEntity = _unit.BrandRepository.Get(brand => brand.Id == id);
            return brandEntity.MapToDTO();
        }

        public void Create(BrandDTO brand)
        {
            var brandEntity = _unit.BrandRepository.Get(b => b.Name == brand.Name);
            if (brandEntity != null)
            {
                return;
            }

            brandEntity = brand.MapToEntity(new Brand());
            _unit.BrandRepository.Create(brandEntity);
            _unit.Commit();
        }

        public void Update(BrandDTO brand)
        {
            if (_unit.BrandRepository.Find(b => b.Name == brand.Name && b.Id != brand.Id).Any())
            {
                return;
            }

            var brandEntity = _unit.BrandRepository.Get(b => b.Id == brand.Id);
            brand.MapToEntity(brandEntity);
            _unit.Commit();
        }

        public void Delete(int id)
        {
            var brand = _unit.BrandRepository.Get(c => c.Id == id);
            _unit.BrandRepository.Delete(brand);
            _unit.Commit();
        }
    }
}