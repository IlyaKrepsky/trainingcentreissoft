﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Mappers;
using DAL.Context.Repositories;
using DAL.Entities.Entities;
using DAL.Interfaces.Interfaces;
using Interfaces.Interfaces;

namespace BLL.Services.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unit;

        public CategoryService(string connectionString)
        {
            _unit = new UnitOfWork(connectionString);
        }

        public IEnumerable<CategoryDTO> GetAll()
        {
            var categories = _unit.CategoryRepository.GetAll();
            return categories.MapToDTOCollection();
        }

        public CategoryDTO Get(int id)
        {
            var category = _unit.CategoryRepository.Get(c => c.Id == id);
            return category.MapToDTO();
        }

        public void Create(CategoryDTO category)
        {
            var categoryEntity = _unit.CategoryRepository.Get(c => c.Name == category.Name);
            if (categoryEntity != null)
            {
                return;
            }

            categoryEntity = category.MapToEntity(new Category());
            _unit.CategoryRepository.Create(categoryEntity);
            _unit.Commit();
        }

        public void Delete(int id)
        {
            var category = _unit.CategoryRepository.Get(c => c.Id == id);
            _unit.CategoryRepository.Delete(category);
            _unit.Commit();
        }

        public void Update(CategoryDTO category)
        {
            if (_unit.CategoryRepository.Find(c => c.Name == category.Name && c.Id != category.Id).Any())
            {
                return;
            }

            var categoryEntity = _unit.CategoryRepository.Get(c => c.Id == category.Id);
            category.MapToEntity(categoryEntity);
            _unit.Commit();
        }
    }
}