﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Helper;
using DAL.Entities.Entities;

namespace BLL.Services.Mappers
{
    public static class BrandMapper
    {
        public static BrandDTO MapToDTO(this Brand brand)
        {
            return brand == null
                ? null
                : new BrandDTO
                {
                    Id = brand.Id,
                    Name = brand.Name,
                    Description = brand.Description
                };
        }

        public static IEnumerable<BrandDTO> MapToDTOCollection(this IEnumerable<Brand> brands)
        {
            return brands?.Select(brand => brand.MapToDTO()).ToList();
        }

        public static Brand MapToEntity(this BrandDTO brandDTO, Brand brand)
        {
            Expect.ArgumentNotNull(brandDTO, nameof(brandDTO));
            brand.Id = brandDTO.Id;
            brand.Name = brandDTO.Name;
            brand.Description = brandDTO.Description;
            return brand;
        }
    }
}