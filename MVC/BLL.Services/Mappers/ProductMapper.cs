﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Helper;
using DAL.Entities.Entities;

namespace BLL.Services.Mappers
{
    public static class ProductMapper
    {
        public static ProductDTO MapToDTO(this Product product)
        {
            return product == null
                ? null
                : new ProductDTO
                {
                    Name = product.Name,
                    Number = product.Number,
                    Cost = product.Cost,
                    Quantity = product.Quantity,
                    Description = product.Description,
                    CategoryName = product.Category.Name,
                    BrandName = product.Brand.Name
                };
        }

        public static Product MapToEntity(this ProductDTO productDto, Product product)
        {
            Expect.ArgumentNotNull(product, nameof(product));
            product.Name = productDto.Name;
            product.Number = productDto.Number;
            product.Cost = productDto.Cost;
            product.Quantity = productDto.Quantity;
            product.Description = productDto.Description;
            return product;
        }

        public static IEnumerable<ProductDTO> MapToDTOCollection(this IEnumerable<Product> products)
        {
            return products?.Select(product => product.MapToDTO()).ToList();
        }
    }
}