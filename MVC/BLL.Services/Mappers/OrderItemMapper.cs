﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Helper;
using DAL.Entities.Entities;

namespace BLL.Services.Mappers
{
    public static class OrderItemMapper
    {
        public static OrderItemDTO MapToDTO(this OrderItem orderItem)
        {
            return orderItem == null? null:new OrderItemDTO
            {
                OrderNumber = orderItem.Order.Number,
                Product = orderItem.Product.MapToDTO(),
                Count = orderItem.ProductCount
            };
        }

        public static IEnumerable<OrderItemDTO> MapToDTOCollection(this IEnumerable<OrderItem> orderItems)
        {
            return orderItems?.Select(order => order.MapToDTO()).ToList();
        }
    }
}