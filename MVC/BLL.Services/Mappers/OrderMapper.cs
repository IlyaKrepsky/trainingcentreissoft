﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using DAL.Entities.Entities;

namespace BLL.Services.Mappers
{
    public static class OrderMapper
    {
        public static OrderDTO MapToDTO(this Order order)
        {
            return order == null
                ? null
                : new OrderDTO
                {
                    Number = order.Number,
                    Date = order.Date,
                    UserEmail = order.User.Email,
                    OrderItems = order.OrderItems.MapToDTOCollection()
                };
        }

        public static IEnumerable<OrderDTO> MapToDTOCollection(this IEnumerable<Order> orders)
        {
            return orders?.Select(order => order.MapToDTO()).ToList();
        }
    }
}