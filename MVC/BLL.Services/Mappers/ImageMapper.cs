﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Helper;
using DAL.Entities.Entities;

namespace BLL.Services.Mappers
{
    public static class ImageMapper
    {
        public static ImageDTO MapToDTO(this Image picture)
        {
            return picture == null
                ? null
                : new ImageDTO
                {
                    Data = picture.Data,
                    Name = picture.Name,
                    ProductNumber = picture.Product.Number,
                    MimeType = picture.MimeType
                };
        }

        public static IEnumerable<ImageDTO> MapToDTOCollection(this IEnumerable<Image> images)
        {
            return images?.Select(image => image.MapToDTO()).ToList();
        }

        public static Image MapToEntity(this ImageDTO image, Image imageEntity)
        {
            Expect.ArgumentNotNull(image, nameof(image));
            imageEntity.Data = image.Data;
            imageEntity.MimeType = image.MimeType;
            imageEntity.Name = image.Name;
            return imageEntity;
        }
    }
}