﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Helper;
using DAL.Entities.Entities;

namespace BLL.Services.Mappers
{
    public static class CategoryMapper
    {
        public static CategoryDTO MapToDTO(this Category category)
        {
            return category == null
                ? null
                : new CategoryDTO
                {
                    Id = category.Id,
                    Name = category.Name,
                    Description = category.Description
                };
        }


        public static IEnumerable<CategoryDTO> MapToDTOCollection(this IEnumerable<Category> categories)
        {
            return categories?.Select(category => category.MapToDTO()).ToList();
        }

        public static Category MapToEntity(this CategoryDTO categoryDTO, Category category)
        {
            Expect.ArgumentNotNull(categoryDTO, nameof(categoryDTO));
            category.Id = categoryDTO.Id;
            category.Name = categoryDTO.Name;
            category.Description = categoryDTO.Description;
            return category;
        }
    }
}