﻿using System.Collections.Generic;
using System.Linq;
using BLL.Models.Models;
using BLL.Services.Helper;
using DAL.Entities.Entities;

namespace BLL.Services.Mappers
{
    public static class UserMapper
    {
        public static UserDTO MapToDTO(this User user)
        {
            return user == null ? null : new UserDTO
            {
                UserName = user.UserName,
                RealName = user.RealName,
                Email = user.Email,
                Phone = user.Phone,
                Roles = user.Roles.Select(r => r.Name).ToList()
            };
        }

        public static IEnumerable<UserDTO> MapToDTOCollection(this IEnumerable<User> users)
        {
            return users?.Select(user => user.MapToDTO()).ToList();
        }

        public static User MapToEntity(this UserDTO userDto, User user)
        {
            Expect.ArgumentNotNull(userDto, nameof(userDto));
            user.Email = userDto.Email;
            user.RealName = userDto.RealName;
            user.UserName = userDto.UserName;
            user.Phone = userDto.Phone;
            if (!string.IsNullOrEmpty(userDto.Password))
            {
                user.Password = SecuredPassword.ComputeHash(userDto.Password);
            }

            return user;
        }
    }
}