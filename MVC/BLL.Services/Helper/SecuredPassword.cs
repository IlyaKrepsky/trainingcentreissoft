﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BLL.Services.Helper
{
    public static class SecuredPassword
    {
        public static byte[] ComputeHash(string input)
        {
            var algorithm = new SHA256CryptoServiceProvider();
            var inputBytes = Encoding.UTF8.GetBytes(input);
            var hashedBytes = algorithm.ComputeHash(inputBytes);
            return hashedBytes;
        }

        public static bool VerifyPassword(string password, byte[] hash)
        {
            if (string.IsNullOrWhiteSpace(password))
                return false;
            var inputBytes = ComputeHash(password);
            return hash.SequenceEqual(inputBytes);
        }
    }
}