﻿using System;

namespace BLL.Services.Helper
{
    public static class Expect
    {
        public static void ArgumentNotNull<T>(T argument, string argumentName) where T : class
        {
            if (argument != null)
            {
                return;
            }
            if (string.IsNullOrEmpty(argumentName))
            {
                throw new ArgumentNullException();
            }
            throw new ArgumentNullException(argumentName);
        }
    }
}