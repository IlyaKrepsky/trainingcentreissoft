﻿using System.Collections.Generic;

namespace DAL.Entities.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string RealName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public byte[] Password { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public User()
        {
            Orders = new List<Order>();
            Roles = new List<Role>();
        }
    }
}