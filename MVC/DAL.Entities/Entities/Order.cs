﻿using System;
using System.Collections.Generic;

namespace DAL.Entities.Entities
{
    public class Order : BaseEntity
    {
        public Guid Number { get; set; }
        public DateTime Date { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }
    }
}