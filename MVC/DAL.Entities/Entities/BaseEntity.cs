﻿namespace DAL.Entities.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}