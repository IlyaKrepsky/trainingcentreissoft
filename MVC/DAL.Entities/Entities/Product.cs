﻿using System;
using System.Collections.Generic;

namespace DAL.Entities.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public Guid Number { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public int Quantity { get; set; }
        public virtual Category Category { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public virtual ICollection<Image> Images { get; set; }

        public Product()
        {
            OrderItems = new List<OrderItem>();
            Images = new List<Image>();
        }
    }
}