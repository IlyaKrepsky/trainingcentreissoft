﻿using System;

namespace DAL.Entities.Entities
{
    public class Image : BaseEntity
    {
        public Guid Name { get; set; }
        public byte[] Data { get; set; }
        public string MimeType { get; set; }
        public virtual Product Product { get; set; }
    }
}