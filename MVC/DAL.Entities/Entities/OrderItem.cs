﻿namespace DAL.Entities.Entities
{
    public class OrderItem : BaseEntity
    {
        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
        public int ProductCount { get; set; }
    }
}