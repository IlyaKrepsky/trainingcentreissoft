﻿using System;

namespace NET01._2.EventHelper
{
    /// <summary>
    ///     Type, that contain event data,
    ///     base type <see cref="EventArgs" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MatrixEventArgs<T> : EventArgs
    {
        /// <summary>
        ///     Matrix's row
        /// </summary>
        public int IndexI { get; }

        /// <summary>
        ///     Matrix's column
        /// </summary>
        public int IndexJ { get; }

        /// <summary>
        ///     Old item value
        /// </summary>
        public T OldValue { get; }

        /// <summary>
        ///     New item value
        /// </summary>
        public T NewValue { get; }

        /// <summary>
        ///     Initializes a new instance of the <c>MatrixEventArgs</c> class.
        /// </summary>
        /// <param name="i">Matrix row</param>
        /// <param name="j">Matrix column</param>
        /// <param name="oldVal">Old item value</param>
        /// <param name="newVal">New item value</param>
        public MatrixEventArgs(int i, int j, T oldVal, T newVal)
        {
            IndexI = i;
            IndexJ = j;
            OldValue = oldVal;
            NewValue = newVal;
        }
    }
}