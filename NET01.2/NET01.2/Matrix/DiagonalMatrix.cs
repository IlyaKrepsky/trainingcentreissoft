﻿using NET01._2.Exception;

namespace NET01._2.Matrix
{
    /// <summary>
    ///     Type, that represent diagonal matrix,
    ///     base type <see cref="SquareMatrix{T}" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        /// <summary>
        ///     Initializes a new instance of the <c>DiagonalMatrix</c> class.
        /// </summary>
        /// <param name="size">Matrix size</param>
        public DiagonalMatrix(int size) : base(size)
        {
            _matrix = new T[size];
        }

        /// <summary>
        ///     Method that check indexes, overrided <see cref="SquareMatrix{T}.CheckIndex" />
        /// </summary>
        /// <exception cref="MatrixException">
        ///     <paramref name="i" /> Must be equal
        ///     <param name="j"></param>
        ///     <paramref name="j" />Must be equal
        ///     <param name="i"></param>
        /// </exception>
        /// <inheritdoc cref="SquareMatrix{T}" />
        /// <param name="i">Row number</param>
        /// <param name="j">Column number</param>
        protected override void CheckIndex(int i, int j)
        {
            base.CheckIndex(i, j);
            if (i != j)
            {
                throw new MatrixException("The element is not on the diagonal (i not equals j)");
            }
        }

        /// <summary>
        ///     Method return index of matrix element,
        ///     overrided <see cref="SquareMatrix{T}.Index"/>
        /// </summary>
        /// <param name="i">Row number</param>
        /// <param name="j">Column nimber</param>
        /// <returns>Index of matrix element</returns>
        protected override int Index(int i, int j)
        {
            return i;
        }

        /// <summary>
        ///     Method return matrix element,
        ///      overrided <see cref="SquareMatrix{T}.GetItem"/>
        /// </summary>
        /// <param name="i">Row number</param>
        /// <param name="j">Column number</param>
        /// <returns>Matrix element</returns>
        protected override T GetItem(int i, int j)
        {
            return i == j ? _matrix[i] : default(T);
        }
    }
}