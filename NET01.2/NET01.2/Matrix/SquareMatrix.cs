﻿using System;
using System.Text;
using NET01._2.EventHelper;

namespace NET01._2.Matrix
{
    /// <summary>
    /// Type, that represent square matrix
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SquareMatrix<T>
    {
        protected T[] _matrix;

        /// <summary>
        /// Event is sending when matrix element changed
        /// </summary>
        public event EventHandler<MatrixEventArgs<T>> ItemChanged;

        /// <summary>
        /// Property, get the matrix size
        /// </summary>      
        public int Size { get; }

        /// <summary>
        /// Assessors, get or set the matrix element
        /// </summary>
        /// <seealso cref="CheckIndex"/>
        /// <seealso cref="Index"/>
        /// <seealso cref="GetItem"/>
        /// <param name="i">Row number</param>
        /// <param name="j">Column number</param>
        /// <returns><see cref="T"/>Matrix element</returns>
        /// <returns></returns>
        public T this[int i, int j]
        {
            get
            {
                CheckIndex(i,j);
                return GetItem(i, j);
            }
            set
            {
                CheckIndex(i, j);
                var obj = GetItem(i,j);
                if (!obj.Equals(value))
                {
                    _matrix[Index(i, j)] = value;
                    ItemChanged?.Invoke(this, new MatrixEventArgs<T>(i, j, obj, value)); 
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <c>SquareMatrix</c> class.
        /// </summary>
        /// <exception cref="ArgumentException">
        /// <paramref name="size" /> must be non-negative. </exception>
        /// <param name="size"></param>
        public SquareMatrix(int size)
        {
            if (size < 0)
            {
                throw new ArgumentException("Size must be non-negative");
            }
            Size = size;
            _matrix = new T[size*size];
        }

        /// <summary>
        /// Ovveride ToString() method
        /// </summary>
        /// <returns>String representation of matrix</returns>
        public override string ToString()
        {
            var matrix = new StringBuilder();
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    matrix.Append(GetItem(i, j) + " ");
                }
                matrix.Append(Environment.NewLine);
            }
            return matrix.ToString();
        }

        /// <summary>
        /// Method that culculate index of matrix element
        /// </summary>
        /// <param name="i">Row number</param>
        /// <param name="j">Column number</param>
        /// <returns>Index of matrix element</returns>
        protected virtual int Index(int i, int j)
        {
            return i * Size + j;
        }

        /// <summary>
        ///     Method return matrix element
        /// </summary>
        /// <param name="i">Row number</param>
        /// <param name="j">Column number</param>
        /// <returns>Matrix element</returns>
        protected virtual T GetItem(int i, int j)
        {
            return _matrix[Index(i, j)];
        }

        /// <summary>
        ///     Method that check indexes
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="i" /> Must be less than matrix size and  non-negative
        ///     <paramref name="j" />Must be less than matrix size and non-negative
        /// </exception>
        /// <param name="i">Row number</param>
        /// <param name="j">Column number</param>
        protected virtual void CheckIndex(int i, int j)
        {
            if (i >= Size || i < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(i), "Must be less than matrix size and non-negative");
            }

            if (j >= Size || j < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(j), "Must be less than matrix size and non-negative");
            }
        }
    }
}