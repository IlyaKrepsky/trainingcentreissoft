﻿using System;

namespace NET01._2.Exception
{
    /// <summary>
    ///     Exception, base type <see cref="ArgumentException"></see>
    /// </summary>
    public class MatrixException : ArgumentException
    {
        /// <summary>
        ///     Initializes a new instance of the <c>MatrixException</c> class.
        /// </summary>
        /// <param name="message"></param>
        public MatrixException(string message) : base(message)
        {
        }
    }
}