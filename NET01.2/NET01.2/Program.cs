﻿using System;
using NET01._2.EventHelper;
using NET01._2.Matrix;

namespace NET01._2
{
    internal class Program
    {
        public static void Handler<T>(object sender, MatrixEventArgs<T> e)
        {
            Console.Write("Method: Value changed: [{0}, {1}] oldValue = {2}, newValue = {3} \n", e.IndexI, e.IndexJ,
                e.OldValue, e.NewValue);
        }

        static void Main(string[] args)
        {
            var rand = new Random();
            var square = new SquareMatrix<int>(10);
            var diagonal = new DiagonalMatrix<int>(15);
            
            diagonal.ItemChanged += Handler;

            diagonal.ItemChanged += (s, e) =>
            {
                Console.Write("Lambda: Value changed:[{0}, {1}] oldValue = {2}, newValue = {3} \n", e.IndexI,
                    e.IndexJ, e.OldValue, e.NewValue);
            };

            diagonal.ItemChanged += delegate(object sender, MatrixEventArgs<int> e)
            {
                Console.Write("Anonymous method: Value changed:[{0}, {1}] oldValue = {2}, newValue = {3} \n",
                    e.IndexI, e.IndexJ, e.OldValue, e.NewValue);
            };

            square.ItemChanged += Handler;

            square.ItemChanged += (s, e) =>
            {
                Console.Write("Lambda:Value changed:[{0}, {1}] oldValue = {2}, newValue = {3} \n", e.IndexI,
                    e.IndexJ, e.OldValue, e.NewValue);
            };

            square.ItemChanged += delegate(object sender, MatrixEventArgs<int> e)
            {
                Console.Write("Anonymous method: Value changed:[{0}, {1}] oldValue = {2}, newValue = {3} \n",
                    e.IndexI, e.IndexJ, e.OldValue, e.NewValue);
            };
            for (var i = 0; i < square.Size; i++)
            {
                for (var j = 0; j < square.Size; j++)
                {
                    square[i, j] = rand.Next(2, 4);
                }
            }
            for (var i = 0; i < diagonal.Size; i++)
            {
                diagonal[i, i] = rand.Next(2, 4);
            }

            Console.WriteLine(diagonal.ToString());
            Console.Write(Environment.NewLine);
            Console.WriteLine(square.ToString());
            Console.ReadKey();
            diagonal[9, 9] = 0;
            Console.ReadKey();
            square[10, 10] = 0;
            Console.ReadKey();
        }
    }
}