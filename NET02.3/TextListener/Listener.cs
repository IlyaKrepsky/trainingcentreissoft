﻿using System.Collections.Generic;
using System.IO;
using Logger.Enums;
using Logger.Interfaces;

namespace TextListener
{
    public class Listener : IListener
    {
        public string Path { get; private set; }

        public void InitProperties(Dictionary<string, string> properties)
        {
            if (!properties.ContainsKey(nameof(Path)))
            {
                throw new KeyNotFoundException("The dictionary doesn't contain the required key \"Path\"");
            }
            Path = properties[nameof(Path)];
        }

        public void Write(string message, LogLevel level)
        {
            using (var log = new StreamWriter(Path))
            {
                log.WriteLine($"{level}: {message}");
            }
        }
    }
}