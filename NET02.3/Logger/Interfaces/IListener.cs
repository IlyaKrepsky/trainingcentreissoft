﻿using System.Collections.Generic;
using Logger.Enums;

namespace Logger.Interfaces
{
    public interface IListener
    {
        void InitProperties(Dictionary<string, string> properties);
        void Write(string message, LogLevel level);
    }
}