﻿using System.Configuration;

namespace Logger.Configuration
{
    [ConfigurationCollection(typeof(ListenerConfiguration), AddItemName = "Listener",
        CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class ListenersCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ListenerConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ListenerConfiguration) element).Name;
        }
    }
}