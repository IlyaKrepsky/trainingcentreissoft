﻿using System.Configuration;

namespace Logger.Configuration
{
    [ConfigurationCollection(typeof(Property), AddItemName = "property",
        CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class PropertyCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Property();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Property) element).Key;
        }
    }
}