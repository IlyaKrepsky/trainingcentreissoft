﻿using System.Configuration;

namespace Logger.Configuration
{
    public class Property : ConfigurationElement
    {
        [ConfigurationProperty("key", IsRequired = true)]
        public string Key
        {
            get => (string) base["key"];
            set => this["key"] = value;
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get => (string) base["value"];
            set => this["value"] = value;
        }
    }
}