﻿using System.Configuration;

namespace Logger.Configuration
{
    public class LoggerSectionConfig : ConfigurationSection
    {
        [ConfigurationProperty("level", IsRequired = true)]
        public string Level
        {
            get => (string) base["level"];
            set => this["level"] = value;
        }

        [ConfigurationProperty("source", IsRequired = true)]
        public string Source
        {
            get => (string) base["source"];
            set => this["source"] = value;
        }

        [ConfigurationProperty("Listeners", IsRequired = true)]
        public ListenersCollection Listeners => (ListenersCollection) base["Listeners"];
    }
}