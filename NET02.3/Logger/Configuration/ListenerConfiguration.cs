﻿using System.Configuration;

namespace Logger.Configuration
{
    public class ListenerConfiguration : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get => (string) base["name"];
            set => this["name"] = value;
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get => (string) base["type"];
            set => this["type"] = value;
        }

        [ConfigurationProperty("Properties", IsRequired = true)]
        public PropertyCollection PropertyCollection => (PropertyCollection) base["Properties"];
    }
}