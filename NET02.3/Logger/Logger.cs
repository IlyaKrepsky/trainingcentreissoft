﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using Logger.Attributes;
using Logger.Configuration;
using Logger.Enums;
using Logger.Interfaces;

namespace Logger
{
    public class Logger
    {
        private readonly LogLevel _level;
        private readonly string _source;
        private readonly LoggerSectionConfig _configuration;
        private readonly List<IListener> _listeners;

        public Logger()
        {
            _configuration = ConfigurationManager.GetSection("LoggerSection") as LoggerSectionConfig;
            if (_configuration == null)
            {
                throw new ArgumentNullException($"LoggerSection", "is not found in config file");
            }
            if (!Enum.TryParse(_configuration.Level, out _level))
            {
                _level = LogLevel.Trace;
            }
            _source = _configuration.Source;
            _listeners = new List<IListener>();
            GetListeners();
        }

        private void GetListeners()
        {
            var listeners = _configuration.Listeners;
            foreach (var listener in listeners)
            {
                try
                {
                    var dictionary = new Dictionary<string, string>();
                    var obj = (ListenerConfiguration) listener;
                    var assemblyName = new AssemblyName(obj.Name);
                    var assembly = Assembly.Load(assemblyName);
                    var type = assembly.GetType(obj.Type);
                    var instance = (IListener) Activator.CreateInstance(type);
                    foreach (var property in obj.PropertyCollection)
                    {
                        dictionary.Add(((Property) property).Key, ((Property) property).Value);
                    }

                    instance.InitProperties(dictionary);
                    _listeners.Add(instance);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public void  Track(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj), "is null");
            }
            
            var type = obj.GetType();
            var entityAttribute = Attribute.GetCustomAttribute(type, typeof(TrackingEntityAttribute));
            if (entityAttribute != null)
            {               
                foreach (var propertyInfo in type.GetProperties())
                {
                    var propertyAttribute = propertyInfo.GetCustomAttribute(typeof(TrackingPropertyAttribute));
                    if (propertyAttribute != null)
                    {
                        _listeners.ForEach(l => l.Write($"{propertyInfo.Name} - {propertyInfo.GetValue(obj)}", LogLevel.Trace));
                    }
                }
                foreach (var fieldInfo in type.GetFields())
                {
                    var fieldAttributes = fieldInfo.GetCustomAttribute(typeof(TrackingPropertyAttribute));
                    if (fieldAttributes != null)
                    {
                        _listeners.ForEach(l => l.Write($"{fieldInfo.Name} - {fieldInfo.GetValue(obj)}", LogLevel.Trace));
                    }
                }
            }  
            
        }

        public void Trace(string message, LogLevel messageLevel)
        {
            if (messageLevel >= _level)
            {
                message = $"{_source} {DateTime.Now.ToString(CultureInfo.CurrentUICulture)}: {message}";
                foreach (var listener in _listeners)
                {
                    listener.Write(message, messageLevel);
                }
            }
        }
    }
}