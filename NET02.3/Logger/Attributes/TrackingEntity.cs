﻿using System;

namespace Logger.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TrackingEntityAttribute : Attribute
    {
    }
}