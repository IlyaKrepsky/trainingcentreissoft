﻿using System.Collections.Generic;
using System.Diagnostics;
using Logger.Enums;
using Logger.Interfaces;

namespace EventLogListener
{
    public class Listener : IListener
    {
        private EventLog _eventLog;
        public string Source { get; private set; }
        public string Log { get; private set; }

        public void InitProperties(Dictionary<string, string> properties)
        {
            if (!properties.ContainsKey(nameof(Source)))
            {
                throw new KeyNotFoundException("The dictionary doesn't contain the required key \"Source\"");
            }

            if (!properties.ContainsKey(nameof(Log)))
            {
                throw new KeyNotFoundException("The dictionary doesn't contain the required key \"Log\"");
            }

            Source = properties[nameof(Source)];
            Log = properties[nameof(Log)];
        }

        public void Write(string message, LogLevel level)
        {
            if (!EventLog.SourceExists(Source))
            {
                EventLog.CreateEventSource(Source, Log);
            }

            _eventLog = new EventLog {Source = Source};

            if ((int) level <= 2)
            {
                _eventLog?.WriteEntry($"{level}: {message}", EventLogEntryType.Information);
            }

            if ((int) level == 3)
            {
                _eventLog?.WriteEntry($"{level}: {message}", EventLogEntryType.Warning);
            }

            if ((int) level > 3)
            {
                _eventLog?.WriteEntry($"{level}: {message}", EventLogEntryType.Error);
            }

            _eventLog.Close();
        }
    }
}