﻿using System;
using System.Diagnostics;
using Logger.Attributes;
using Logger.Enums;

namespace NET02._3
{
    internal class Program
    {
        private static Logger.Logger _logger;

        private static void Main(string[] args)
        {
            var test = new TestClass(5) {TestProperty = "I'm Test"};
            var sw = new Stopwatch();
            sw.Start();
            _logger = new Logger.Logger();
            _logger.Track(test);

            _logger.Trace("Hello I'm Logger", LogLevel.Trace);
            _logger.Trace("Hello I'm Logger", LogLevel.Error);
            _logger.Trace("Hello I'm Logger", LogLevel.Fatal);
            _logger.Trace("Hello I'm Logger", LogLevel.Info);
            _logger.Trace("Hello I'm Logger, Info", LogLevel.Info);
            _logger.Trace("Hello I'm Logger, Fatal", LogLevel.Fatal);
            sw.Stop();
            Console.WriteLine((sw.ElapsedMilliseconds / 100.0).ToString());
            Console.ReadKey();
        }
    }

    [TrackingEntity]
    internal class TestClass
    {
        [TrackingProperty] public string TestProperty { get; set; }
        [TrackingProperty] public int _testField;

        public TestClass(int testField)
        {
            _testField = testField;
        }
    }
}