﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Logger.Enums;
using Logger.Interfaces;
using Microsoft.Office.Interop.Word;

namespace WordListener
{
    public class Listener : IListener
    {
        private ApplicationClass _wordApp;
        private Document _wordDoc;
        private readonly object _missing = Missing.Value;

        public string Path { get; private set; }

        public void InitProperties(Dictionary<string, string> properties)
        {
            if (!properties.ContainsKey(nameof(Path)))
            {
                throw new KeyNotFoundException("The dictionary doesn't contain the required key \"Path\"");
            }
            Path = Directory.GetCurrentDirectory() + @"\" + properties[nameof(Path)];
        }

        public void Write(string message, LogLevel level)
        {
            _wordApp = new ApplicationClass {Visible = false};
            _wordDoc = new DocumentClass();
            var docx = new FileInfo(Path);
            if (!docx.Exists)
            {
                _wordDoc = _wordApp.Documents.Add();
                _wordDoc.SaveAs(Path, WdSaveFormat.wdFormatDocumentDefault);
                _wordDoc.Activate();
            }
            else
            {
                _wordDoc = _wordApp.Documents.Open(Path, _missing, false);
                _wordDoc.Activate();
            }
            _wordDoc.Content.Text += $"{level}: {message}";
            _wordDoc.Save();
            _wordDoc.Close();
            _wordApp.Quit();
        }
    }
}