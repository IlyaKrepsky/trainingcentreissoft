﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using DataLayer.DataModels;
using DataLayer.Interfaces;

namespace DataLayer.Repositories
{
    public class XmlRepository : IRepository
    {
        private static readonly string _path = "sensors.xml";

        public IEnumerable<SensorDataModel> GetSensors()
        {
            var fileInfo = new FileInfo(_path);
            if (fileInfo.Exists)
            {
                using (var stream = new StreamReader(_path))
                {
                    var serializer = new XmlSerializer(typeof(List<SensorDataModel>));
                    return serializer.Deserialize(stream) as IEnumerable<SensorDataModel>;
                }
            }
            return new List<SensorDataModel>();
        }

        public void SaveSensors(IEnumerable<SensorDataModel> listSensors)
        {
            using (var writer = new StreamWriter(_path))
            {
                var serializer = new XmlSerializer(listSensors.GetType());
                serializer.Serialize(writer, listSensors);
                writer.Flush();
            }
        }
    }
}