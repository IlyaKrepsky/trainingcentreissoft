﻿using System;
using DataLayer.Interfaces;

namespace DataLayer.Repositories
{
    public class FactoryRepositories
    {
        public IRepository FactoryMethod(int type)
        {
            switch (type)
            {
                case 0:
                {
                    return new XmlRepository();
                }
                case 1:
                {
                    return new JsonRepository();
                }
                default: throw new ArgumentException("Invalid storage type");
            }
        }
    }
}