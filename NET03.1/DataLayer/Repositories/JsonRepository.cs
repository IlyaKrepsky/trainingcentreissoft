﻿using System.Collections.Generic;
using System.IO;
using DataLayer.DataModels;
using DataLayer.Interfaces;
using Newtonsoft.Json;

namespace DataLayer.Repositories
{
    public class JsonRepository : IRepository
    {
        private static readonly string _path = "sensors.json";

        public IEnumerable<SensorDataModel> GetSensors()
        {
            var fileInfo = new FileInfo(_path);
            if (fileInfo.Exists)
            {
                using (var file = File.OpenText(_path))
                {
                    var serializer = new JsonSerializer();
                    return (IEnumerable<SensorDataModel>) serializer.Deserialize(file,
                        typeof(IEnumerable<SensorDataModel>));
                }
            }

            return new List<SensorDataModel>();
        }

        public void SaveSensors(IEnumerable<SensorDataModel> listSensors)
        {
            using (var file = File.CreateText(_path))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(file, listSensors);
            }
        }
    }
}