﻿namespace DataLayer.DataModels
{
    public enum SensorTypeModel
    {
        Pressure,
        Temperature,
        Speed
    }
}