﻿using System;

namespace DataLayer.DataModels

{
    public class SensorDataModel
    {
        public Guid Id { get; set; }
        public SensorTypeModel Type { get; set; }
        public int Interval { get; set; }
    }
}