﻿using System.Collections.Generic;
using DataLayer.DataModels;

namespace DataLayer.Interfaces
{
    public interface IRepository
    {
        IEnumerable<SensorDataModel> GetSensors();
        void SaveSensors(IEnumerable<SensorDataModel> listSensors);
    }
}