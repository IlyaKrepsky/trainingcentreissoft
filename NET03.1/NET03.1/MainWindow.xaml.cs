﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using NET03.View;
using NET03.ViewModels;

namespace NET03._1
{
    public partial class MainWindow : Window
    {
        public int _userChoice;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var messageWindow = new MessageWindow {Owner = this};
            var result = messageWindow.ShowDialog();
            if (result == true)
            {
                DataContext = new SensorListVm(_userChoice);
            }
            else
            {
                Close();
            }
        }

        private void Sensors_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListBoxItem))
                Sensors.UnselectAll();
        }
    }
}