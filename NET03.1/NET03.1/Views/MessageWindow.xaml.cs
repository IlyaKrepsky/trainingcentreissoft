﻿using System.Windows;
using NET03._1;

namespace NET03.View
{
    public partial class MessageWindow : Window
    {
        public MessageWindow()
        {
            InitializeComponent();
        }

        private void XmlButton_OnClick(object sender, RoutedEventArgs e)
        {
            var main = Owner as MainWindow;
            main._userChoice = 0;
            DialogResult = true;
            Close();
        }

        private void JsonButton_OnClick(object sender, RoutedEventArgs e)
        {
            var main = Owner as MainWindow;
            main._userChoice = 1;
            DialogResult = true;
            Close();
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}