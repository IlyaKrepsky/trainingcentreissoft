﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using BusinessLayer.BusinessModels;
using BusinessLayer.Manager;
using NET03.Commands;

namespace NET03.ViewModels
{
    public class SensorListVm : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly SensorsManager _manager;
        private SensorVm _selectedItem;
        private SensorVm _newItem;
        private Command _add;
        private Command _delete;
        private Command _save;

        public ObservableCollection<SensorVm> SensorCollection { get; set; }
        public IEnumerable<string> TypeList { get; }

        public SensorVm SelectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;
                NotifyPropertyChanged(nameof(SelectedItem));
            }
        }

        public SensorVm NewItem
        {
            get => _newItem;
            set
            {
                _newItem = value;
                NotifyPropertyChanged(nameof(NewItem));
            }
        }

        public Command Add => _add ?? (_add = new Command(obj =>
        {
            var sensor = new Sensor(NewItem.SensorInterval, NewItem.SensorType);
            NewItem = new SensorVm(new Sensor(0));
            SensorCollection.Add(new SensorVm(sensor));
        }, o => NewItem.SensorInterval > 0 && NewItem.SensorInterval <= 1000));

        public Command Delete => _delete ?? (_delete = new Command(
                                     obj => { SensorCollection.Remove(_selectedItem); },
                                     obj => SelectedItem != null));


        public Command Save => _save ?? (_save = new Command(obj =>
        {
            var sensorList = SensorCollection.Select(s => s._sensor);
            _manager.SaveAll(sensorList);
        }));


        public SensorListVm(int storageType)
        {
            _manager = new SensorsManager(storageType);
            SensorCollection = new ObservableCollection<SensorVm>();
            foreach (var sensor in _manager.GetAll())
            {
                SensorCollection.Add(new SensorVm(sensor));
            }

            TypeList = Enum.GetValues(typeof(SensorType))
                .Cast<SensorType>()
                .Select(v => v.ToString()).ToList();
            NewItem = new SensorVm(new Sensor(0));
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}