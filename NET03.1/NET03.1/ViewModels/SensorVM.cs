﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using BusinessLayer.BusinessModels;
using BusinessLayer.Interfaces;
using NET03.Commands;

namespace NET03.ViewModels
{
    public class SensorVm : IObserver, INotifyPropertyChanged
    {
        public readonly Sensor _sensor;
        public event PropertyChangedEventHandler PropertyChanged;

        private double _value;
        private int _interval;
        private string _mode;
        private string _type;
        private Command _changeMode;

        public string Id => _sensor.Id.ToString();


        public double SensorValue
        {
            get => _value;
            set
            {
                _value = value;
                NotifyPropertyChanged(nameof(SensorValue));
            }
        }

        public int SensorInterval
        {
            get => _interval;
            set
            {
                _interval = value;
                NotifyPropertyChanged(nameof(SensorInterval));
            }
        }

        public string SensorMode
        {
            get => _mode;
            set
            {
                _mode = value;
                NotifyPropertyChanged(nameof(SensorMode));
            }
        }

        public string SensorType
        {
            get => _type;
            set
            {
                _type = value;
                NotifyPropertyChanged(nameof(SensorType));
            }
        }

        public Command ChangeMode => _changeMode ?? (_changeMode = new Command(obj =>
        {
            _sensor.ChangeMode();
            SensorMode = _sensor.Mode.ModeName;
        }));

        public void Update(object o)
        {
            SensorValue = (int) o;
        }

        public SensorVm(Sensor sensor)
        {
            _sensor = sensor;
            _type = sensor.Type.ToString();
            _interval = sensor.Interval;
            _mode = sensor.Mode.ModeName;
            sensor.RegisterObserver(this);
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}