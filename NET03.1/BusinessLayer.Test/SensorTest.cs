﻿using BusinessLayer.BusinessModels;
using BusinessLayer.Modes;
using NUnit.Framework;

namespace BusinessLayer.Test
{
    [TestFixture]
    public class SensorTest
    {
        [Test]
        public void ChangeMode_ModeChanged()
        {
            //Arrange
            var sensor = new Sensor(1);
            var firstMode = new ModePause(sensor).ModeName;
            var secondMode = new ModeCalibr(sensor).ModeName;
            var thirdMode = new ModeWork(sensor).ModeName;
            //Assert
            Assert.That(sensor.Mode.ModeName, Is.EqualTo(firstMode));
            sensor.ChangeMode();
            Assert.That(sensor.Mode.ModeName, Is.EqualTo(secondMode));
            sensor.ChangeMode();
            Assert.That(sensor.Mode.ModeName, Is.EqualTo(thirdMode));
            sensor.ChangeMode();
            Assert.That(sensor.Mode.ModeName, Is.EqualTo(firstMode));
        }
    }
}