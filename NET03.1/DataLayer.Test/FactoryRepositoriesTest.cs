﻿using DataLayer.Repositories;
using NUnit.Framework;

namespace DataLayer.Test
{
    [TestFixture]
    public class FactoryRepositoriesTest
    {
        [Test]
        public void FactoryMetod_SetValidValue_ReturnSensorList()
        {
            //Arrange
            var factoryRepo = new FactoryRepositories();
            var jsonRepo = new JsonRepository();
            var xmlRepo = new XmlRepository();
            //Act
            var jsonSensorList = factoryRepo.FactoryMethod(1).GetSensors();
            var xmlSensorList = factoryRepo.FactoryMethod(0).GetSensors();
            //Assert
            Assert.That(() => jsonRepo.GetSensors(), Is.EquivalentTo(jsonSensorList));
            Assert.That(() => xmlRepo.GetSensors(), Is.EqualTo(xmlSensorList));
        }

        [Test]
        public void FactoryMetod_SetInvalidValue_ThrowException()
        {
            //Arrange
            var factoryRepo = new FactoryRepositories();
            //Assert
            Assert.That(() => factoryRepo.FactoryMethod(-1), Throws.ArgumentException);
        }
    }
}