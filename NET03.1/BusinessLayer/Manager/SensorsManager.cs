﻿using System;
using System.Collections.Generic;
using BusinessLayer.BusinessModels;
using DataLayer.DataModels;
using DataLayer.Interfaces;
using DataLayer.Repositories;

namespace BusinessLayer.Manager
{
    public class SensorsManager
    {
        private readonly List<Sensor> _sensors;
        private readonly IRepository _factoryMethod;

        public SensorsManager(int type)
        {
            _factoryMethod = new FactoryRepositories().FactoryMethod(type);
            _sensors = new List<Sensor>();
        }

        public List<Sensor> GetAll()
        {
            foreach (var item in _factoryMethod.GetSensors())
            {
                var sensor = new Sensor(item.Type.ToString(), item.Interval, item.Id);
                _sensors.Add(sensor);
            }

            return _sensors;
        }

        public void SaveAll(IEnumerable<Sensor> sensors)
        {
            var sensorsList = new List<SensorDataModel>();
            foreach (var item in sensors)
            {
                sensorsList.Add(new SensorDataModel
                {
                    Id = Guid.Parse(item.Id.ToString()),
                    Interval = item.Interval,
                    Type = (SensorTypeModel) Enum.Parse(typeof(SensorTypeModel),
                        item.Type.ToString())
                });
            }

            _factoryMethod.SaveSensors(sensorsList);
        }
    }
}