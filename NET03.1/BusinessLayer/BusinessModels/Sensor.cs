﻿using System;
using System.Collections.Generic;
using BusinessLayer.GeneratorId;
using BusinessLayer.Interfaces;
using BusinessLayer.Modes;

namespace BusinessLayer.BusinessModels
{
    public class Sensor : IObservable
    {
        private readonly List<IObserver> _obsevers;
        private int _value;

        public Guid Id { get; }
        public SensorType Type { get; }
        public int Interval { get; }
        public Mode Mode { get; internal set; }

        public int Value
        {
            get => _value;
            internal set
            {
                if (_value != value)
                {
                    _value = value;
                    NotifyObservers();
                }
            }
        }

        public Sensor(int interval, string type = null)
        {
            var idGenerator = IdGenerator.GetInstance();
            Id = idGenerator.GetGuid();
            Type = string.IsNullOrEmpty(type) ? SensorType.Pressure : (SensorType) Enum.Parse(typeof(SensorType), type);
            Interval = interval;
            Mode = new ModePause(this);
            _obsevers = new List<IObserver>();
        }

        internal Sensor(string type, int interval, Guid id)
        {
            Type = (SensorType) Enum.Parse(typeof(SensorType), type);
            Interval = interval;
            Id = Guid.Parse(id.ToString());
            Mode = new ModePause(this);
            _obsevers = new List<IObserver>();
        }

        public void ChangeMode()
        {
            Value = 0;
            Mode.ChangeMode();
        }

        public void RegisterObserver(IObserver o)
        {
            _obsevers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            _obsevers.Remove(o);
        }

        public void NotifyObservers()
        {
            _obsevers.ForEach(o => o.Update(_value));
        }
    }
}