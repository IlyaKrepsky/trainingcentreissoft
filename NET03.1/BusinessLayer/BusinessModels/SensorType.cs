﻿namespace BusinessLayer.BusinessModels
{
    public enum SensorType
    {
        Pressure,
        Temperature,
        Speed
    }
}