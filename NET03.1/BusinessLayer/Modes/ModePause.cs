﻿using BusinessLayer.BusinessModels;

namespace BusinessLayer.Modes
{
    public class ModePause : Mode
    {
        public override string ModeName => "Pause";

        public ModePause(Sensor sensor) : base(sensor)
        {
        }

        protected override void ChangeState()
        {
            _sensor.Mode = new ModeCalibr(_sensor);
        }
    }
}