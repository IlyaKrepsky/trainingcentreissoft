﻿using System.Threading;
using BusinessLayer.BusinessModels;

namespace BusinessLayer.Modes
{
    public class ModeCalibr : Mode
    {
        public override string ModeName => "Calibration";

        public ModeCalibr(Sensor sensor) : base(sensor)
        {
            _timer = new Timer(obj => _sensor.Value++, null, 0, 1000);
        }

        protected override void ChangeState()
        {
            _sensor.Mode = new ModeWork(_sensor);
        }
    }
}