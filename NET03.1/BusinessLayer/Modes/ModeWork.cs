﻿using System;
using System.Threading;
using BusinessLayer.BusinessModels;

namespace BusinessLayer.Modes
{
    public class ModeWork : Mode
    {
        public override string ModeName => "Work";

        public ModeWork(Sensor sensor) : base(sensor)
        {
            _timer = new Timer(obj => _sensor.Value = new Random().Next(0, 100), null, 0, _sensor.Interval * 1000);
        }

        protected override void ChangeState()
        {
            _sensor.Mode = new ModePause(_sensor);
        }
    }
}