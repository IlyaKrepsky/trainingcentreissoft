﻿using System.Threading;
using BusinessLayer.BusinessModels;

namespace BusinessLayer.Modes
{
    public abstract class Mode
    {
        protected Timer _timer;
        protected Sensor _sensor;
        public virtual string ModeName { get; }

        protected Mode(Sensor sensor)
        {
            _sensor = sensor;
        }

        public void ChangeMode()
        {
            _timer?.Dispose();
            ChangeState();
        }

        protected abstract void ChangeState();
    }
}