﻿namespace BusinessLayer.Interfaces
{
    public interface IObserver
    {
        void Update(object o);
    }
}