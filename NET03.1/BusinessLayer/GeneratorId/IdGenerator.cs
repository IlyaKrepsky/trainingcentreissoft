﻿using System;

namespace BusinessLayer.GeneratorId
{
    public sealed class IdGenerator
    {
        private static IdGenerator _instance;
        private static readonly object _lockObj = new object();

        private IdGenerator()
        {
        }

        public Guid GetGuid()
        {
            return Guid.NewGuid();
        }

        public static IdGenerator GetInstance()
        {
            lock (_lockObj)
            {
                if (_instance == null)
                {
                    _instance = new IdGenerator();
                }
            }

            return _instance;
        }
    }
}