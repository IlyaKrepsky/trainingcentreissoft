﻿using NET02._1.Helpers;
using NUnit.Framework;

namespace UnitTest
{
    [TestFixture]
    internal class HelperTest
    {
        [Test]
        public void RewriteName_SetInvertedName_RewriteString()
        {
            //Arrange
            var name = "Galt, John";
            //Act
            var anotherName = name.RewriteName();
            //Assert
            Assert.That(anotherName, Is.Not.Null);
            Assert.That(anotherName, Is.EqualTo("John Galt"));
        }

        [Test]
        public void RewriteName_SetDirectName_DontRewriteString()
        {
            //Arrange
            var name = "Galt John";
            //Act
            var anotherName = name.RewriteName();
            //Assert
            Assert.That(anotherName, Is.Not.Null);
            Assert.That(anotherName, Is.EqualTo("Galt John"));
        }

        [Test]
        public void CompareKeys_SetValidValues_ReturnTrue()
        {
            //Arrange
            var name = "John Gald";
            var anotherName = "Gald, John";
            //Act
            var flag = name.CompareKeys(anotherName);
            //Arrange
            Assert.That(flag, Is.True);
        }

        [Test]
        public void CompareKeys_SetInvalidValues_ReturnFalse()
        {
            //Arrange
            var name = "John Gald";
            var anotherName = "Gald, Rick";
            //Act
            var flag = name.CompareKeys(anotherName);
            //Arrange
            Assert.That(flag, Is.False);
        }
    }
}