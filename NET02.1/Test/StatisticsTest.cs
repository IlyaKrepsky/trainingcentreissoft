﻿using System;
using System.Collections.Generic;
using System.Linq;
using NET02._1.Entities;
using NUnit.Framework;

namespace UnitTest
{
    [TestFixture]
    public class StatisticsTest
    {
        private Statistics GetStatistic()
        {
            var a1 = new Answer("Answer1", 5);
            var a2 = new Answer("Answer2", 10);

            var q1 = new List<Answer> {a1.True(), a2.False()};
            var q3 = new List<Answer> {a1.True(), a2.True()};

            var t1 = new Test("Test 1", "John Galt", new DateTime(2017, 12, 10), q1);
            var t2 = new Test("Test 2", "John Galt", new DateTime(2017, 12, 10), q3);
            var t3 = new Test("Test 2", "Vasia Pupkin", new DateTime(2012, 10, 10), q3);

            return new Statistics
            {
                ["John Galt"] = new List<Test> {t1, t2},
                ["Vasia Pupkin"] = new List<Test> {t3}
            };
        }

        [Test]
        public void GetPassedTests__statistics_Return_Person1_2_Person2_1()
        {
            //Arrange
            var statistic = GetStatistic();
            //Act 
            var passedTests = statistic.GetPassedTests();
            //Assert
            Assert.That(passedTests, Is.Not.Null);
            Assert.That(passedTests.Keys.Count, Is.EqualTo(2));
            Assert.That(passedTests["John Galt"], Is.EqualTo(2));
            Assert.That(passedTests["Vasia Pupkin"], Is.EqualTo(1));
        }

        [Test]
        public void FindSimplyTest__statistics_Return_Test2()
        {
            //Arrange 
            var statistic = GetStatistic();
            //Act 
            var test = statistic.FindSimplestTest();
            //Assert
            Assert.That(test, Is.Not.Null);
            Assert.That(test.TestName, Is.EqualTo("Test 2"));
        }

        [Test]
        public void PersonCountTrueAnswers__statistics_Return_2Group()
        {
            //Arrange
            var statistic = GetStatistic();
            // Act
            var result = statistic.PersonCountTrueAnswers();
            //Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(2));
            Assert.That(result["Test 2"].Count(), Is.EqualTo(2));
            Assert.That(result["Test 1"].Count(), Is.EqualTo(1));
            Assert.That(result["Test 1"].ToList()[0].Key, Is.EqualTo("John Galt"));
            Assert.That(result["Test 1"].ToList()[0].Value, Is.EqualTo(1));
        }

        [Test]
        public void FindHardestTestQuestion__statistics_Return_Answer2()
        {
            //Arange
            var statistic = GetStatistic();
            //Act
            var question = statistic.FindHardestTestQuestion();
            //Assert
            Assert.That(question, Is.EqualTo("Answer2"));
        }


        [Test]
        public void Accessor_SetNewValidTestList_SetNewValue()
        {
            //Arrange
            var statistic = GetStatistic();
            var list = new List<Test>
            {
                new Test("Test", "Li, Yan", DateTime.Now, new List<Answer>
                {
                    new Answer("Answer", 15)
                })
            };
            //Act


            //Assert
            Assert.That(() => statistic["Yan Li"] = list, Throws.Nothing);
            Assert.That(statistic["Li, Yan"], Is.EqualTo(list));
            Assert.That(statistic["Yan Li"].Count, Is.EqualTo(1));
        }

        [Test]
        public void Accessor_SetNewInValidTestList_ThrowException()
        {
            //Arrange
            var statistic = GetStatistic();
            var list = new List<Test>
            {
                new Test("Test", "Li, Yan", DateTime.Now, new List<Answer>
                {
                    new Answer("Answer", 15)
                })
            };
            //Assert
            Assert.That(() => statistic["Li, Li"] = list, Throws.ArgumentException);
        }


        [Test]
        public void Accessor_GetValueByInvalidKey_ThrowException()
        {
            //Assert
            Assert.That(() => GetStatistic()["Li, Li"], Throws.Exception);
        }

        [Test]
        public void Accessor_GetValueByValidKey_ReturnValue()
        {
            //Assert
            Assert.That(GetStatistic()["John Galt"].Count, Is.EqualTo(2));
            //Assert
            Assert.That(GetStatistic()["John Galt"][0].TestName, Is.EqualTo("Test 1"));
        }
    }
}