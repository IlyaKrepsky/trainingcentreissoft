﻿using System;
using NET02._1.Helpers;
using NUnit.Framework;

namespace UnitTest
{
    [TestFixture]
    internal class ExpectText
    {
        [Test]
        public void ArgumentIsMatchToPattern_InvalidString_ExceptionThrows()
        {
            //Assert
            Assert.Throws<ArgumentException>(() => Expect.ArgumentIsMatchToPattern("Vasily"));
            Assert.Throws<ArgumentException>(() => Expect.ArgumentIsMatchToPattern("VASILY"));
        }

        [Test]
        public void ArgumentIsMatchToPattern_ValidString_ExceptionNotThrows()
        {
            //Assert
            Assert.DoesNotThrow(() => Expect.ArgumentIsMatchToPattern("Vasily Dashkevich"));
            Assert.DoesNotThrow(() => Expect.ArgumentIsMatchToPattern("Vasily, Dashkevich"));
        }
    }
}