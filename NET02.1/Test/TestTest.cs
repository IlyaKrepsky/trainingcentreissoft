﻿using System;
using System.Collections.Generic;
using System.Linq;
using NET02._1.Entities;
using NUnit.Framework;

namespace UnitTest
{
    [TestFixture]
    internal class TestTest
    {
        [Test]
        public void GetEnumerator_CompareAnswerFactors_True()
        {
            //Arrange
            var a1 = new Answer("Answer1", 5);
            var a2 = new Answer("Answer2", 150);
            var a3 = new Answer("Answer3", 100);
            var a4 = new Answer("Answer4", 1);
            var quiz = new List<Answer> {a1.True(), a2.False(), a3.True(), a4.False()};
            var test = new Test("Test", "John Galt", DateTime.Now, quiz);
            //Act
            var result = test.Enumerator().ToList();
            //Assert
            Assert.That(result[0].Factor, Is.EqualTo(a2.Factor));
            Assert.That(result[1].Factor, Is.EqualTo(a3.Factor));
            Assert.That(result[2].Factor, Is.EqualTo(a1.Factor));
            Assert.That(result[3].Factor, Is.EqualTo(a4.Factor));
        }

        [Test]
        public void Constructor_CallWithInvalidValues_ExceptionThrow()
        {
            Assert.That(() => new Test("", "John Galt", DateTime.Now, new List<Answer>()),
                Throws.ArgumentNullException);
            Assert.That(() => new Test(" ", "John Galt", DateTime.Now, null), Throws.ArgumentNullException);
        }
    }
}