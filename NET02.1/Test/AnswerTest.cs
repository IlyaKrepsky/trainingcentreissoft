﻿using NET02._1.Entities;
using NUnit.Framework;

namespace UnitTest
{
    [TestFixture]
    internal class AnswerTest
    {
        [Test]
        public void Constructor_CallWithInvalidValues_ExceptionThrow()
        {
            Assert.That(() => new Answer(string.Empty, 5), Throws.ArgumentNullException);
            Assert.That(() => new Answer("Answer", -5), Throws.ArgumentException);
        }
    }
}