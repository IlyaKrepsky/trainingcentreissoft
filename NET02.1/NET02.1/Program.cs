﻿using System;
using System.Collections.Generic;
using NET02._1.Entities;

namespace NET02._1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var a1 = new Answer("Answer1", 5);
            var a2 = new Answer("Answer2", 10);
            var a3 = new Answer("Answer3", 15);
            var a4 = new Answer("Answer4", 1);
            var a5 = new Answer("Answer5", 2);
            var a6 = new Answer("Answer6", 3);

            var q1 = new List<Answer> {a1.True(), a2.False(), a3.True(), a4.True()};
            var q2 = new List<Answer> {a1.True(), a2.True(), a3.True(), a4.True()};
            var q3 = new List<Answer> {a3.True(), a4.True(), a6.True(), a5.True()};
            var q4 = new List<Answer> {a3.False(), a4.True(), a6.True(), a5.False()};

            var t1 = new Test("Test 1", "John Galt", new DateTime(2017, 12, 10), q1);
            var t2 = new Test("Test 2", "John Galt", new DateTime(2017, 12, 10), q3);
            var t3 = new Test("Test 2", "Vasia Pupkin", new DateTime(2012, 10, 10), q3);
            var t4 = new Test("Test 3", "Vasia Pupkin", new DateTime(2017, 12, 10), q2);
            var t5 = new Test("Test 2", "Kohno, Anastasia", new DateTime(2017, 12, 10), q4);
            var t6 = new Test("Test 3", "Kohno, Anastasia", new DateTime(2017, 12, 10), q1);

            var statitic = new Statistics
            {
                ["John Galt"] = new List<Test> {t1, t2},
                ["Vasia Pupkin"] = new List<Test> {t3, t4},
                ["Kohno, Anastasia"] = new List<Test> {t5, t6}
            };

            var name = "Nikolay Petrov";
            statitic[name] = new List<Test>
            {
                new Test("Test 5", name, DateTime.Now, new List<Answer>
                {
                    a1.False(),
                    a3.True(),
                    a2.False()
                })
            };

            Console.WriteLine(statitic.ToString());

            Console.ReadKey();

            foreach (var t in statitic.GetPassedTests())
            {
                Console.WriteLine(t.ToString());
            }

            Console.ReadKey();

            Console.WriteLine(statitic.FindSimplestTest().TestName);

            Console.ReadKey();
            Console.WriteLine(statitic.FindHardestTestQuestion());
            Console.ReadKey();
            var query = statitic.PersonCountTrueAnswers();

            foreach (var test in query)
            {
                Console.WriteLine(test.Key);
                foreach (var value in test)
                {
                    Console.WriteLine(value.Key + ":" + value.Value);
                }
            }


            Console.ReadKey();

            foreach (var item in t1.Enumerator())
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadKey();
            Console.WriteLine(t1.ToString());

            Console.ReadKey();
        }
    }
}