﻿using System;
using NET02._1.Helpers;

namespace NET02._1.Entities
{
    public class Answer : IComparable
    {
        public string AnswerText { get; }
        public int Factor { get; }
        public bool Flag { get; }

        public Answer(string text, int factor, bool flag = false)
        {
            Expect.ArgumentNotNull(text);
            Factor = factor <= 0 ? throw new ArgumentException("Must be positive", nameof(factor)) : factor;
            AnswerText = text;
            Flag = flag;
        }

        public override string ToString()
        {
            return $"{AnswerText}  {Factor}  {Flag}";
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            if (!(obj is Answer answer))
            {
                throw new ArgumentException("Object is not a Answer");
            }

            return Factor.CompareTo(answer.Factor);
        }

        public Answer True()
        {
            return new Answer(AnswerText, Factor, true);
        }

        public Answer False()
        {
            return new Answer(AnswerText, Factor);
        }
    }
}