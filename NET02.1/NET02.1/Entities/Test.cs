﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using NET02._1.Helpers;

namespace NET02._1.Entities
{
    public class Test : IEnumerable<Answer>
    {
        private readonly List<Answer> _answers;

        public string TestName { get; }
        public string PersonName { get; }
        public DateTime Date { get; }

        public Test(string testName, string personName, DateTime date, List<Answer> answers)
        {
            Expect.ArgumentNotNull(testName);
            Expect.ArgumentIsMatchToPattern(personName);
            _answers = answers ?? throw new ArgumentNullException(nameof(_answers), "is null");
            TestName = testName;
            PersonName = personName;
            Date = date;
        }

        public IEnumerable<Answer> Enumerator()
        {
            var list = new List<Answer>();
            list.AddRange(_answers);
            list.Sort();
            list.Reverse();
            foreach (var answer in list)
            {
                yield return answer;
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendLine($"{TestName}  {PersonName}  {Date.ToShortDateString()}");
            foreach (var answ in _answers)
            {
                result.AppendLine(answ.ToString());
            }

            return result.ToString();
        }


       public IEnumerator<Answer> GetEnumerator()
        {
            return _answers.GetEnumerator();
        }


       IEnumerator IEnumerable.GetEnumerator()
        {
           return GetEnumerator();
        }
    }
}