﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NET02._1.Helpers;

namespace NET02._1.Entities
{
    public class Statistics
    {
        private readonly Dictionary<string, List<Test>> _statistic;

        public List<Test> this[string personName]
        {
            get
            {
                var key = personName.RewriteName();
                if (!_statistic.ContainsKey(key))
                {
                    throw new KeyNotFoundException("key does not exist in the statistics");
                }

                return _statistic[key];
            }
            set
            {
                var key = personName.RewriteName();
                foreach (var t in value)
                {
                    if (!key.CompareKeys(t.PersonName))
                    {
                        throw new ArgumentException($"Name {t.PersonName} does not match to key {key}");
                    }
                }

                if (!_statistic.ContainsKey(key))
                {
                    _statistic.Add(key, new List<Test>());
                }

                _statistic[key] = value;
            }
        }

        public Statistics()
        {
            _statistic = new Dictionary<string, List<Test>>();
        }


        /// <summary>
        ///     Get a set of pairs of the type "human name - the number of tests passed".
        /// </summary>
        public Dictionary<string, int> GetPassedTests()
        {
            return (from s in _statistic
                select new {Name = s.Key, s.Value.Count}).ToDictionary(t => t.Name, c => c.Count);
        }


        /// <summary>
        ///     Find the test that passed the largest number of people.
        /// </summary>
        /// <returns>
        ///     <see cref="Test" />
        /// </returns>
        public Test FindSimplestTest()
        {
            var q2 = _statistic.Keys.SelectMany(t => _statistic[t], (t, s) => new {t, s});

            var group = q2.GroupBy(t => t.s.TestName);
            var count = group.Max(c => c.Count());
            return (group.FirstOrDefault(g => g.Count() == count) ?? throw new InvalidOperationException())
                .Select(t => t.s).FirstOrDefault();
        }


        /// <summary>
        ///     Output pairs of the kind "human name - the number of correct answers"
        ///     for each test present in the statistics.
        /// </summary>
        public Lookup<string, KeyValuePair<string, int>> PersonCountTrueAnswers()
        {
            var q2 = _statistic.Keys.SelectMany(t => _statistic[t], (t, s) => new {t, s});
            return (Lookup<string, KeyValuePair<string, int>>) q2.Join(_statistic.Keys, a => a.t, k => k, (t, k) =>
                new
                {
                    Person = k,
                    t.s.TestName,
                    Count = t.s.Count(a => a.Flag)
                }).ToLookup(k => k.TestName, value => new KeyValuePair<string, int>(value.Person, value.Count));
        }


        /// <summary>
        ///     Find the test question in the statistics with the least number of correct answers.
        /// </summary>
        /// <returns>
        ///     <see cref="string" />
        /// </returns>
        public string FindHardestTestQuestion()
        {
            var allAnswer = _statistic.Keys.SelectMany(t => _statistic[t], (t, s) => new {s})
                .SelectMany(t => t.s, (t, a) => new {a}).Select(a => a.a).Where(a => a.Flag);
            var group = allAnswer.GroupBy(a => a.AnswerText);
            return group.FirstOrDefault(a => a.Count(f => f.Flag) == group.Min(b => b.Count()))
                .FirstOrDefault().AnswerText;
        }


        public override string ToString()
        {
            var result = new StringBuilder();
            foreach (var item in _statistic)
            {
                result.AppendLine(item.Key);

                foreach (var t in item.Value)
                {
                    result.Append(t);
                }
            }

            return result.ToString();
        }
    }
}