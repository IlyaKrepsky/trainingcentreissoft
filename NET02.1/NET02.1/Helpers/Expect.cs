﻿using System;
using System.Text.RegularExpressions;

namespace NET02._1.Helpers
{
    public static class Expect
    {
        public static void ArgumentNotNull(string paramName)
        {
            if (string.IsNullOrEmpty(paramName))
            {
                throw new ArgumentNullException(nameof(paramName), "is null");
            }
        }

        public static void ArgumentIsMatchToPattern(string name)
        {
            ArgumentNotNull(name);
            var regex = new Regex(@"^([A-Z][a-z]*(,)? [A-Z][a-z]*$)");
            if (!regex.IsMatch(name))
            {
                throw new ArgumentException(
                    "Name does not match to pattern \"FirstName SecondName\" or \"SeconName, FirstName\" ");
            }
        }
    }
}