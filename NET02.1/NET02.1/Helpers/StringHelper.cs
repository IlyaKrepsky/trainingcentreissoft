﻿using System;

namespace NET02._1.Helpers
{
    public static class Helper
    {
        public static string RewriteName(this string name)
        {
            Expect.ArgumentIsMatchToPattern(name);
            if (name.Contains(", "))
            {
                var index = name.IndexOf(", ", StringComparison.Ordinal);
                return name.Substring(index + 2) + " " + name.Substring(0, index);
            }

            return name;
        }

        public static bool CompareKeys(this string key, string anotherString)
        {
            if (anotherString.Contains(", "))
            {
                var anotherIndex = anotherString.IndexOf(", ", StringComparison.Ordinal);
                var index = key.IndexOf(" ", StringComparison.Ordinal);
                return anotherString.Substring(anotherIndex + 2) == key.Substring(0, index) &&
                       anotherString.Substring(0, anotherIndex) == key.Substring(index + 1);
            }

            return key.Equals(anotherString);
        }
    }
}